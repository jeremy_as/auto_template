PREREQUIS : Java 64 bit

INIT :

1 - COPY MULTISHAPE.bat in a WORKING DIRECTORY and LAUNCH IT (double click)

2 - EDIT config.xml (in you want to change input/output path)

3 - LAUNCH MULTISHAPE.bat again (to create input/ouput path hierarchy)


USAGE :

1 - ADD INPUT FILES in input subdirectories (body_origin, body_target, shooting)

2 - LAUNCH MULTISHAPE.bat again to prepare and apply transformations

3 - CONFIGURE transformations in core/[algoName]/[transformation_name]/

4 - TO RELAUNCH transformations (after configuration change) :
	4.1 - DELETE core/[algoName]/[transformation_name].bat file(s) 
	4.2 - LAUNCH MULTISHAPE.bat again
	
	
CONFIGURATION (USAGE 3-): Add the files in core/[algoName]/[transformation_name]/
BIGWARP :   core/bigwarp/[transformation_name]/[transformation_name]_landmarks.csv
			core/bigwarp/[transformation_name]/[transformation_name]_arm_left_landmarks.csv (Optional)
			core/bigwarp/[transformation_name]/[transformation_name]_arm_right_landmarks.csv (Optional)
			
BUNWARP :   core/bunwarp/[transformation_name]/[transformation_name]_landmarks.txt (Optional)
			core/bunwarp/[transformation_name]/[transformation_name]_arm_left_landmarks.txt (Optional)
			core/bunwarp/[transformation_name]/[transformation_name]_arm_right_landmarks.txt (Optional)
parameters for preparation in : core/bunwarp/[transformation_name].bat
example: 0 3 0 0.1 0.1 1 10
//startPrecision endPrecision maxSubSamplingFactor DivergenceW CurlW ImageW ConsistencyW
// see https://imagej.net/BUnwarpJ#Input.2FOutput_options for parameters infos

PUPPET : 	core/puppet/[transformation_name]/T1....TXX.tif 
			core/puppet/[transformation_name]/[transformation_name].psb 
