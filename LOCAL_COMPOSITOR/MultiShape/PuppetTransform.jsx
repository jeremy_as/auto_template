
var ATN_MULTISHAPE_FILE = 'AS_MULTISHAPE.atn';
var ATN_MULTISHAPE_ACTION = 'CLIPPING_UPDATE';

function PuppetTransform()
{
	this.init = function(width, height)
	{
		this.width = width;
		this.height = height;
	}
	
	//Open each transformation file (.tif), and apply to shooting file)
	this.apply = function(transPath, shootPath, outputPath)
	{
		var files = file_get_all(transPath, '*' + FILE_EXT_TIF);
		if (files.length == 0)
		{
			LogError('Puppet: missing transformation files for ' + transPath)
			return false;
		}

		//Sort files
		files.sort(function(a, b)
		{
			return parseInt(a.name.substring(1)) - parseInt(b.name.substring(1));
		});

		var outputDir = outputPath.substring(0, outputPath.lastIndexOf('\\') + 1);
		var clipPath = shootPath.replace(STR_SHOOT, STR_CLIP);
		for (var i = 0; i < files.length; i++)
		{
			var fileName = files[i].name;
			var clipName = fileName.substring(0, fileName.length - 4) + STR_CLIP + FILE_EXT_TIF;
			
			//Copy transformation to output dir
			files[i].copy(outputDir + fileName);
			files[i].copy(outputDir + clipName);
			
			//Load image in first transformation
			if (i == 0)
			{
				ok &= this.applyStep(new File(outputDir + fileName), shootPath);
				ok &= this.applyStep(new File(outputDir + clipName), clipPath);
			}
			//Update content for other ones
			else
			{
				ok &= this.applyStep(new File(outputDir + fileName), '', true);
				ok &= this.applyStep(new File(outputDir + clipName), '_clipping', true);
			}
		}
		
		return ok;
	}
	
	this.applyStep = function(filePath, shootPath, updateContent)
	{		
		Log('Puppet: opening step ' + filePath);
		var file = new File(filePath);
		if (!file.exists)
		{
			LogError(filePath + ' not found');
			return false;
		}
		
		//Open tif
		app.displayDialogs = DialogModes.NO;//TEST
		
		app.activeDocument = open(file);
		if (app.activeDocument.layers.length < 2 || app.activeDocument.layers[1].typename != 'ArtLayer')
		{
			LogError("Puppet: shooting layer not found");
			close_document();
			return false;
		}
		
		//Redim
		var needResize = this.width > 0 && this.height > 0 && (app.activeDocument.width.as('px') != this.width || app.activeDocument.height.as('px') != this.height);
		if (needResize)
			app.activeDocument.resizeCanvas(UnitValue(this.width, "px"), UnitValue(this.height, "px"), AnchorPosition.MIDDLECENTER);
		
		var layer = app.activeDocument.layers[0];
		
		//Update content or replace image
		if (updateContent === true)
		{
			var outputPath = file.fullName.substring(0, file.fullName.lastIndexOf('/') + 1);
			var tifName = layer.name;
			var file = new File(outputPath + tifName + shootPath + FILE_EXT_TIF);		
			layer_open_png(layer, file);
		}
		else
		{
			var file = new File(shootPath);		
			layer_open_png(layer, file);
		}
		
		//Save
		app.activeDocument.save();
		
		close_document();
		return true;
	};
}