///////////////////////////////////////////////////////////////////////////////
// DEFINES
///////////////////////////////////////////////////////////////////////////////

//XML conf file
var CONF_FILE = 'config.xml';

//Input directories
var INPUT_BODY_ORIGIN = 'body_origin';
var INPUT_BODY_TARGET = 'body_target';
var INPUT_SHOOTING = 'shooting';

//Shoot & body detection
var STR_SHOOT = '_shoot';
var STR_CLIP = '_clipping';
var STR_CLIPPING = '_clipping';
var STR_BODY = STR_CLIPPING + '_body';
var STR_ARM = STR_CLIPPING + '_arm';
var STR_ARM_LEFT = STR_ARM + '_left';
var STR_ARM_RIGHT = STR_ARM + '_right';
var ARM_LEFT = '_arm_left';
var ARM_RIGHT = '_arm_right';

//Algorithm list
var DIR_CORE = 'core/';
var DIR_BUNWARP = 'bunwarp/';
var DIR_MATHLAB = 'mathlab/';
var DIR_COREG = 'coreg/';
var DIR_LDDMM = 'lddmm/';
var DIR_BIGWARP = 'bigwarp/';
var DIR_PUPPET = 'puppet/';
var SUBDIRS_CORE = [DIR_BUNWARP, DIR_MATHLAB, DIR_COREG, DIR_LDDMM, DIR_BIGWARP, DIR_PUPPET];
var ALGO_NAME = ['Bunwarp', 'Mathlab', 'Coreg', 'LDDMM', 'Bigwarp', 'Puppet'];
var PUPPET_INDEX = 5;

//Output psb generation
var ATN_MULTISHAPE_FILE = 'AS_MULTISHAPE.atn';
var ALGO_ACTIONS = ['BUNWARP CLIPPING UPDATE', '', '', '', '', 'CLIPPING_UPDATE'];


//Used by Bunwarp & Bigwarp
var FIJI_PATH = LOCAL_COMPOSITOR + 'MultiShape/fiji/';

//BunWarp CMD
var BUNWARP_CMD = 'java -Xmx13312m -cp "' 
			+ FIJI_PATH + 'jars/ij-1.51n.jar;' 
			+ FIJI_PATH + 'plugins/bUnwarpJ_-2.6.8.jar" bunwarpj.bUnwarpJ_';
var BUNWARP_PARAMS = '0 3 0 0.1 0.1 1 10';	
//startPrecision endPrecision maxSubSamplingFactor DivergenceW CurlW ImageW ConsistencyW
// see https://imagej.net/BUnwarpJ#Input.2FOutput_options for parameters infos

//BunWarp with landmarks (for arms)
var BUNWARP_LANDMARKS_PARAMS = '0 4 0 0.01 0.01 0 0'; //image weight = 0, ConsistencyW = 0
var BUNWARP_LANDMARKS_OPTS = '-landmark 1.0'; //landmark weight = 1.0


//BigWarp
var BIGWARP_CMD = 'java -Xmx13312m -cp "'
			+ FIJI_PATH + 'jars/ij-1.51n.jar;' 
			+ FIJI_PATH + 'jars/jitk-tps-2.1.0.jar;'
			+ FIJI_PATH + 'jars/log4j-1.2.17.jar;'
			+ FIJI_PATH + 'jars/imglib2-3.2.1.jar;'
			+ FIJI_PATH + 'jars/fiji-2.0.0-SNAPSHOT.jar;'
			+ FIJI_PATH + 'plugins/bigdataviewer_fiji-3.0.0.jar;'
			+ FIJI_PATH + 'plugins/bigwarp_fiji-2.1.5.jar" bigwarp.BigWarpBatchTransform';

//Mathlab
//TODO

//Used by Coreg & LDDMM
var TIPL_PATH = LOCAL_COMPOSITOR + 'MultiShape/tipl/entry_point.exe';

//Coreg CMD
var COREG_CMD = 'DEACTIVATED"' + TIPL_PATH + '" 0';

//LDDMM
var LDDMM_CMD = 'DEACTIVATED"' + TIPL_PATH + '" 1';


function MultiShape()
{
	//Members (read in xml)
	this.inputPath = '';
	this.outputPath = '';
	
	//Inputs
	this.bodyOrigins = []; //Array of jpg files
	this.bodyTargets = [];	//Array of jpg files
	this.shootings = [];	//Same
	
	this.puppetTransform = new PuppetTransform(); // Puppet transform instance
	
	//External Configuration
	this.setInputPath = function(inputPath) { this.inputPath = inputPath; };
	this.setOutputPath = function(outputPath) { this.outputPath = outputPath; };
	
	//Launch
	this.start = function()
	{
		if (!this.loadXml())
		{
			LogError('FATAL XML ERROR');
			return false;
		}
		
		if (!this.checkDirectories())
		{
			LogError('FATAL DIRECTORIES ERROR');
			return false;
		}

		if (!this.getInputs())
		{
			LogError('FATAL INPUTS ERROR');
			return false;
		}
		
		return this.launchProcess();
	}

	//Read xml config
	this.loadXml = function()
	{
		//Read local config
		var config = new File(CONF_FILE);
		if (!config.open('r'))
		{
			LogError('FATAL ERROR OPENING LOCAL CONF FILE ' + CONF_FILE);
			return false;
		}
		var content = config.read();
		config.close();
		
		//Parse xml
		var xmlDoc = new XML(content);

		//Read path attributes
		this.inputPath = path_format(xmlDoc.@inputPath, true);
		this.outputPath = path_format(xmlDoc.@outputPath, true);
		
		//Read dimensions
		this.width = parseInt(xmlDoc.@width); if (isNaN(this.width)) this.width = 0;
		this.height = parseInt(xmlDoc.@height); if (isNaN(this.height)) this.height = 0;
		this.puppetTransform.init(this.width, this.height);
		return true;
	};
	
	//Check directories and create missing ones
	this.checkDirectories = function()
	{
		//Main dirs
		if (!dir_create(this.inputPath) || !dir_create(this.outputPath) || !dir_create(DIR_CORE))
		{
			LogError('error creating input/output or core path ' + this.inputPath + ', ' + this.outputPath);
			return false;
		}
		//Input subdirs
		if (!dir_create(this.inputPath + INPUT_BODY_ORIGIN) || !dir_create(this.inputPath + INPUT_BODY_TARGET) || !dir_create(this.inputPath + INPUT_SHOOTING))
		{
			LogError('error creating input subdirectories');
			return false;
		}
		//Create subdirs in core and ouput dirs
		for (var i = 0; i < SUBDIRS_CORE.length; i++)
		{
			var subdir = SUBDIRS_CORE[i];
			if (!dir_create(DIR_CORE + subdir) || !dir_create(this.outputPath + subdir))
			{
				LogError('error creating subdirectory ' + subdir);
				return false;
			}
		}
		return true;
	};
	
	//Get input files
	this.getInputs = function()
	{
		//Read input body files
		this.bodyOrigins = file_get_all(this.inputPath + INPUT_BODY_ORIGIN, '*' + FILE_EXT_JPG);
		if (this.bodyOrigins.length === 0)
		{
			LogError('no files found in ' + this.inputPath + INPUT_BODY_ORIGIN);
			return false;
		}
		
		//Read target bodies files
		this.bodyTargets = file_get_all(this.inputPath + INPUT_BODY_TARGET, '*' + FILE_EXT_JPG);
		if (this.bodyTargets.length === 0)
		{
			LogError('no files found in ' + this.inputPath + INPUT_BODY_TARGET);
			return false;
		}
		
		//Read shooting files
		this.shootings = file_get_all(this.inputPath + INPUT_SHOOTING, '*' + FILE_EXT_TIF);
		if (this.shootings.length === 0)
		{
			LogError('no shooting found in ' + this.inputPath + INPUT_SHOOTING);
			return false;
		}
		
		return true;
	};
	
	//Filter file list to get clipping_type
	this.getClipping = function(files, type)
	{
		for (var i = 0; i < files.length; i++)
		{
			var name = files[i].name;
			if (name.indexOf('_clipping_' + type) >= 0)
				return files[i];
		}
		return null;
	};
	
	
	//LAUNCH PROCESS
	this.launchProcess = function()
	{	
		Log('parsing ' + this.bodyOrigins.length + ' files in body_origin, ' 
			+ this.bodyTargets.length + ' files in body_target and ' 
			+ this.shootings.length + ' files in shooting');
		
		//Prepare images
		var ok = this.prepareImages(this.bodyOrigins);
		ok &= this.prepareImages(this.bodyTargets);
		ok &= this.prepareImages(this.shootings);
		ok &= this.prepareImages(file_get_all(this.inputPath + INPUT_BODY_TARGET, '*' + FILE_EXT_TIF), true);
		
		//Prepare transformations
		var bodyClip = this.getClipping(this.bodyOrigins, "body");
		if (bodyClip === null)
		{
			LogError('clipping_body not found for body origin');
			return false;
		}
		
		if (!ok)
			LogError('error preparing inputs');
		
		//Prepare transformations bodyOrigin -> bodyTargets
		for (var i = 0; i < this.bodyTargets.length; i++)
		{
			var bodyTarget = this.bodyTargets[i];
			
			//Clipping body
			if (bodyTarget.name.indexOf(STR_BODY) >= 0)
				ok &= this.prepareTransformation(bodyClip, bodyTarget);
		}
		
		if (!ok)
			LogError('error preparing transformations');
		
		//Apply transformations on shootings
		for (var s = 0; s < this.shootings.length; s++)
		{
			var shooting = this.shootings[s];
			for (var i = 0; i < this.bodyTargets.length; i++)
			{
				var bodyTarget = this.bodyTargets[i];
				
				//Clipping body
				if (bodyTarget.name.indexOf(STR_BODY) >= 0)
					ok &= this.applyTransformation(bodyClip, bodyTarget, shooting);
			}
		}
		
		return ok;
	};

	//Prepare image files
	this.prepareImages = function(imageFiles, isModelTif)
	{
		var ok = true;
		for (var i = 0; i < imageFiles.length; i++)
			ok &= this.prepareImage(imageFiles[i], isModelTif);
		return ok;
	}
	
	//Prepare image file (resize images and make bmp files)
	this.prepareImage = function(imageFile, isModelTif)
	{
		var imagePath = imageFile.fullName.substring(0, imageFile.fullName.length - 4);
		
		var tifPath = imageFile.fullName.substring(imageFile.fullName.length - 4) == FILE_EXT_TIF ? imageFile.fullName : '';
		
		//Check png (not needed any more)
		var pngPath = imagePath + FILE_EXT_PNG;
		//var pngFile = new File(pngPath);
		//if (pngFile.exists)
			pngPath = ''; // already done
		
		//Check bmp
		var bmpPath = imagePath + FILE_EXT_BMP;
		var bmpFile = new File(bmpPath);
		if (isModelTif || bmpFile.exists)
			bmpPath = ''; // already done
		
		//Convert
		var needConvert = isModelTif || pngPath != '' || bmpPath != '';
		var w = this.width > 0 ? this.width : null;
		var h = this.height > 0 ? this.height : null;
		if (needConvert && !convert_image(imageFile, tifPath, pngPath, bmpPath, w, h))
		{
			LogError('error converting imageFile ' + imageFile.fsName);
			return false;
		}
		
		return true;
	}

	
	//PREPARE TRANSFORMATION
	this.prepareTransformation = function(bodyOrigin, bodyTarget)
	{
		var bodyOriginName = bodyOrigin.name.substring(0, bodyOrigin.name.indexOf(STR_BODY));
		var bodyTargetName = bodyTarget.name.substring(0, bodyTarget.name.indexOf(STR_BODY));
		var transName = bodyOriginName + '_' + bodyTargetName;
		
		//Get origin & target arms
		var oArmLeft = new File(bodyOrigin.fullName.replace(STR_BODY, STR_ARM_LEFT));
		var oArmRight = new File(bodyOrigin.fullName.replace(STR_BODY, STR_ARM_RIGHT));
		var armLeft = new File(bodyTarget.fullName.replace(STR_BODY, STR_ARM_LEFT));
		var armRight = new File(bodyTarget.fullName.replace(STR_BODY, STR_ARM_RIGHT));
		
		//Launch all algorithms
		for (var i = 0; i < SUBDIRS_CORE.length; i++)
		{
			var subdir = SUBDIRS_CORE[i];
			var transPath = new Folder(DIR_CORE + subdir + transName).fsName;
			var batFile = new File(transPath + '.bat');
			if (batFile.exists)
				continue; // already computed
		
			Log(subdir + ': Preparing tranformation ' + bodyOriginName + ' -> ' + bodyTargetName);

			//Get prepare command
			var cmd = this['getCmdPrepare'+ALGO_NAME[i]](bodyOrigin, bodyTarget, transPath, transName);
			
			//Get arms command
			var cmdArmLeft = '';
			var cmdArmRight = '';
			if (armLeft.exists)
				cmdArmLeft = this['getCmdPrepare'+ALGO_NAME[i]](oArmLeft, armLeft, transPath, transName + ARM_LEFT);
			if (armRight.exists)
				cmdArmRight = this['getCmdPrepare'+ALGO_NAME[i]](oArmRight, armRight, transPath, transName + ARM_RIGHT);
			
			if (cmd == '' && cmdArmLeft == '' && cmdArmRight == '')
			{
				Log(subdir + ': nothing to do, manual preparation');
				continue;
			}
			
			cmd += '\n' + cmdArmLeft + '\n' + cmdArmRight;
			
			//Create batch & launch it
			launch_batch(batFile, cmd);
			Log(subdir + ': Tranformation prepared ' + transName);
		}
					
		return true;
	};
	
	this.getCmdPrepareBunwarp = function(bodyOrigin, bodyTarget, transPath, transName)
	{
		//Create subdirectory for optional landmarks
		if (transName.indexOf(STR_ARM) < 0)
		{
			var folder = new Folder(transPath);
			if (!folder.exists)
				folder.create();
		}
		
		var landMarks = new File(transPath + '/' + transName + '_landmarks.txt');
		if (landMarks.exists)
		{
			Log('using landmarks for ' + transName + ', skipping prepare');
			return '';
		}
		
		transPath = transPath.substring(0, transPath.lastIndexOf('\\') + 1) + transName;
		var transInvert = transPath + '_invert';
		var bodyOriginPath = bodyOrigin.fsName.substring(0, bodyOrigin.fsName.length - 4) + FILE_EXT_BMP;
		var bodyTargetPath = bodyTarget.fsName.substring(0, bodyTarget.fsName.length - 4) + FILE_EXT_BMP;
		return BUNWARP_CMD + ' -align "' + bodyTargetPath + '" NULL "' + bodyOriginPath + '" NULL ' + BUNWARP_PARAMS + ' "' + transPath + FILE_EXT_TIF + '" "' + transInvert + FILE_EXT_TIF + '" -save_transformation';
	};
	
	this.getCmdPrepareBigwarp = function(bodyOrigin, bodyTarget, transPath, transName)
	{
		//Create subdirectory for transformation
		if (transName.indexOf(STR_ARM) < 0)
		{
			var folder = new Folder(transPath);
			if (!folder.exists)
				folder.create();
		}
		
		//Manual preparation (no batch)
		return '';
	};
	
	this.getCmdPrepareMathlab = function(bodyOrigin, bodyTarget, transPath, transName)
	{
		//MANUAL PREPARATION (no batch)
		return '';
	};
	
	this.getCmdPrepareCoreg = function(bodyOrigin, bodyTarget, transPath, transName)
	{
		//Replace jpg by bmp
		var bodyOriginPath = bodyOrigin.fsName.substring(0, bodyOrigin.fsName.length - 4) + FILE_EXT_BMP;
		var bodyTargetPath = bodyTarget.fsName.substring(0, bodyTarget.fsName.length - 4) + FILE_EXT_BMP;
		return cmd = COREG_CMD + ' 0 "' + bodyOriginPath + '" "' + bodyTargetPath + '" "' + transPath + '"';
	};
	
	this.getCmdPrepareLDDMM = function(bodyOrigin, bodyTarget, transPath, transName)
	{
		//Replace jpg by bmp
		var bodyOriginPath = bodyOrigin.fsName.substring(0, bodyOrigin.fsName.length - 4) + FILE_EXT_BMP;
		var bodyTargetPath = bodyTarget.fsName.substring(0, bodyTarget.fsName.length - 4) + FILE_EXT_BMP;
		return cmd = LDDMM_CMD + ' 0 "' + bodyOriginPath + '" "' + bodyTargetPath + '" "' + transPath + '"';
	};
	
	this.getCmdPreparePuppet = function(bodyOrigin, bodyTarget, transPath, transName)
	{
		//Create subdirectory for transformation
		if (transName.indexOf(STR_ARM) < 0)
		{
			var folder = new Folder(transPath);
			if (!folder.exists)
				folder.create();
		}
		
		//Manual preparation (no batch)
		return '';
	};
	
	
	//APPLY TRANSFORMATION
	this.applyTransformation = function(bodyOrigin, bodyTarget, shooting)
	{
		var bodyOriginName = bodyOrigin.name.substring(0, bodyOrigin.name.indexOf(STR_BODY));
		var bodyTargetName = bodyTarget.name.substring(0, bodyTarget.name.indexOf(STR_BODY));
		var transName = bodyOriginName + '_' + bodyTargetName;
		
		var shootName = shooting.name.substring(0, shooting.name.length - 4);
		var outputName = transName + '_' + shootName;
		var index = outputName.indexOf(STR_SHOOT); if (index < 0) index = outputName.indexOf(STR_CLIP);
		var shootOutput = outputName.substring(0, index);
				
		//Launch all algorithms
		var ok = true;
		for (var i = 0; i < SUBDIRS_CORE.length; i++)
		{
			var subdir = SUBDIRS_CORE[i];
			var transPath = new Folder(DIR_CORE + subdir + transName).fsName;
			var outputDir = new Folder(this.outputPath + subdir + shootOutput);
			var outputPath = outputDir.fsName + '\\' + outputName;
			var batFile = new File(transPath + '_' + shootName + '.bat');
			if (batFile.exists) // already computed
				continue; 
							
			Log(subdir + ': Transformation launched ' + transName + ' for ' + shootName);

			//Create output dir
			if (!outputDir.exists)
				outputDir.create();		
			
			//Get body command
			var cmd = this['getCmdApply'+ALGO_NAME[i]](transPath, transName, shooting, outputPath);
			
			//Get arms command
			var cmdArmLeft = this['getCmdApply'+ALGO_NAME[i]](transPath, transName + ARM_LEFT, shooting, outputPath + ARM_LEFT);
			var cmdArmRight = this['getCmdApply'+ALGO_NAME[i]](transPath, transName + ARM_RIGHT, shooting, outputPath + ARM_RIGHT);
			
			if (cmd == '' && cmdArmLeft == '' && cmdArmRight == '')
			{
				Log(subdir + ': nothing to do');
				continue;
			}
			
			cmd += '\n' + cmdArmLeft + '\n' + cmdArmRight;
			
			//Create batch & launch it
			launch_batch(batFile, cmd);
			Log(subdir + ': Tranformation done ' + transName + ' for ' + shootName);
			
			//Make psb
			if (shootName.indexOf(STR_SHOOT) >= 0 && file_get_all(outputDir.fsName).length > 0)
			{
				Log(subdir + ': Making PSB ' + outputDir.fsName + '\\' + shootOutput + FILE_EXT_PSB);
				var psbOk = this.createFinalPSB(transPath, outputDir.fsName, shootOutput, bodyTarget, transName, ALGO_ACTIONS[i], i == PUPPET_INDEX);
				ok &= psbOk;
				if (!psbOk)
					LogError(subdir + ': error making PSB');
			}
		}
		
		return ok;
	};
	
	this.getCmdApplyBunwarp = function(transPath, transName, shooting, outputPath)
	{	
		//Work better with bmp
		var shootPath = shooting.fsName.substring(0, shooting.fsName.length - 4) + FILE_EXT_BMP;
		
		var landMarks = new File(transPath + '/' + transName + '_landmarks.txt');
		if (landMarks.exists)
		{
			Log('Bunwarp: using landmarks to apply transformation ' + transName);
			var outputInvert = outputPath + '_invert';
			return BUNWARP_CMD + ' -align "' + shootPath + '" NULL "' + shootPath + '" NULL ' + BUNWARP_LANDMARKS_PARAMS + ' "' + outputPath + FILE_EXT_TIF + '" "' +  outputInvert + FILE_EXT_TIF + '" ' + BUNWARP_LANDMARKS_OPTS + ' "' + landMarks.fsName + '"';
		}
		
		transPath = transPath.substring(0, transPath.lastIndexOf('\\') + 1) + transName;
		if (! (new File(transPath + '_transf.txt').exists))
		{
			LogError('Bunwarp: missing transformation file ' + transPath + '_transf.txt');
			return '';
		}
		
		return BUNWARP_CMD + ' -elastic_transform "' + shootPath + '" "' + shootPath + '" "' + transPath + '_transf.txt" "' + outputPath + FILE_EXT_TIF + '"';
	};
	
	this.getCmdApplyBigwarp = function(transPath, transName, shooting, outputPath)
	{
		var landMarks = new File(transPath + '/' + transName + '_landmarks.csv');
		if (!landMarks.exists)
		{
			LogError('Bigwarp: missing landmarks ' + landMarks.fsName);
			return '';
		}
		
		//<num-dimensions> <landmarks-file> <moving-image-file> <template-image-file> <destination-file>
		return BIGWARP_CMD + ' 2 "' + landMarks.fsName + '" "' + shooting.fsName + '" "' + shooting.fsName + '" "' + outputPath + FILE_EXT_TIF + '"';
	};
	
	this.getCmdApplyMathlab = function(transPath, transName, shooting, outputPath)
	{
		//TODO
		return '';
	};
	
	this.getCmdApplyCoreg = function(transPath, transName, shooting, outputPath)
	{
		return COREG_CMD + ' 1 "' + transPath + '.txt" "' + shooting.fsName + '" "' + outputPath + FILE_EXT_BMP + '"';
	};
	
	this.getCmdApplyLDDMM = function(transPath, transName, shooting, outputPath)
	{
		return LDDMM_CMD + ' 1 "' + transPath + '.txt" "' + shooting.fsName + '" "' + outputPath + FILE_EXT_BMP + '"';
	};
	
	this.getCmdApplyPuppet = function(transPath, transName, shooting, outputPath)
	{
		if (transName.indexOf(ARM_LEFT) >= 0 || transName.indexOf(ARM_RIGHT) >= 0)
			return '';
		
		if (shooting.fullName.indexOf(STR_CLIP) >= 0)
			return '';
		
		this.puppetTransform.apply(transPath, shooting.fullName, outputPath);
		return 'echo done';
	};
	
	
	//FINAL PSB CREATION
	this.createFinalPSB = function(transPath, outputPath, shootOutput, bodyTarget, transName, actionName, isPuppet)
	{
		//Open final psd
		var psb = null;
		if (isPuppet)
			psb = new File(transPath + '/' + transName + FILE_EXT_PSB);
		else
			psb = new File(this.inputPath + INPUT_BODY_TARGET + '/' + bodyTarget.name.substring(0, bodyTarget.name.indexOf(STR_BODY)) + FILE_EXT_PSB);
			
		if (!psb.exists)
		{
			LogError('PSB: Error psb not found : ' + psb.fsName);
			return false;
		}
				
		//Copy body tif & shadow
		var ok = true;
		var bodyPath = bodyTarget.fullName.substring(0, bodyTarget.fullName.indexOf(STR_BODY));
		var bodyFile = new File(bodyPath + FILE_EXT_TIF);
		var shadowFile = new File(bodyPath + '_shadow' + FILE_EXT_TIF);
		if (!bodyFile.exists || !bodyFile.copy(outputPath + '/' + bodyFile.name))
		{
			LogError('PSB: Error copying ' + bodyFile.fsName + ' to ' + outputPath + '/' + bodyFile.name);
			ok = false;
		}
		if (!shadowFile.exists || !shadowFile.copy(outputPath + '/' + shadowFile.name))
		{
			LogError('PSB: Error copying ' + shadowFile.fsName + ' to ' + outputPath + '/' + shadowFile.name);
			ok = false;
		}
		
		var outputPsb = outputPath + '/' + shootOutput + FILE_EXT_PSB;		
		
		//Open psb and update layers
		if (isPuppet)
		{
			psb.copy(outputPsb);
			app.activeDocument = open_psb(new File(outputPsb));
			//layer_update_all_modified(); // <---- This cause huge lag and memory issues
		}
		else
		{
			app.activeDocument = open_psb(psb);
		}
		
		this.needResize = this.width > 0 && this.height > 0 && (app.activeDocument.width.as('px') != this.width || app.activeDocument.height.as('px') != this.height);
		if (this.needResize)
		{
			this.fx = 100 * this.width / app.activeDocument.width.as('px');
			this.fy = 100 * this.height / app.activeDocument.height.as('px');
			app.activeDocument.resizeCanvas(UnitValue(this.width, "px"), UnitValue(this.height, "px"), AnchorPosition.MIDDLECENTER);
		}
		
		this.parseLayers(app.activeDocument, outputPath, shootOutput, bodyTarget.name.substring(0, bodyTarget.name.indexOf(STR_BODY)));
		
		//Launch action update mask
		var actionPath = ATN_MULTISHAPE_FILE; 
		if (!action_load(actionPath))
		{
			LogError('FATAL ERROR LOADING ACTION -> ' + actionPath + ' not found');
			ok = false;
		}
		//Launch compo action
		else if (!action_exec(actionName, ATN_MULTISHAPE_FILE))
		{
			LogError('FATAL ERROR EXEC ACTION -> ' + actionName + ' in ' + ATN_MULTISHAPE_FILE);
			ok = false;
		}
		
		//Make sure model is visible (action seems to hide it)
		try { 
			app.activeDocument.layers.getByName('MODEL').visible = true;
		}
		catch(e) {}
		
		//Save output
		if (isPuppet)
			app.activeDocument.save();
		else
			save_psb(outputPsb);
		
		close_document();
		
		//Delete unused files
		var invertFiles = file_get_all(outputPath, '*_invert.tif');
		for (var i = 0; i < invertFiles.length; i++)
			invertFiles[i].remove();
		
		return ok;
	};
	
	this.parseLayers = function(parent, outputPath, shootOutput, bodyName)
	{
		if (!parent.hasOwnProperty('layers'))
			return;
		
		for (var i = 0; i < parent.layers.length; i++)
		{
			var layer = parent.layers[i];
			if (layer.typename == 'ArtLayer')
				this.replaceLayerImage(layer, outputPath, shootOutput, bodyName);
			//else if (layer.typename == 'LayerSet')
			//	this.resizeLayer(layer);
			
			this.parseLayers(layer, outputPath, shootOutput, bodyName);
		}
	};

	this.replaceLayerImage = function(layer, outputPath, shootOutput, bodyName)
	{
		var name = layer.name.toLowerCase();

		var img = '';
		if (name == 'model')
			img = bodyName + FILE_EXT_TIF;
		else if (name == 'shadow')
			img = bodyName + '_shadow' + FILE_EXT_TIF;
		else if ('_' + name == STR_SHOOT + ARM_LEFT)
			img = shootOutput + STR_SHOOT + ARM_LEFT + FILE_EXT_TIF;
		else if ('_' + name == STR_CLIP + ARM_LEFT)
			img = shootOutput + STR_CLIP + ARM_LEFT + FILE_EXT_TIF;
		else if ('_' + name == STR_SHOOT + ARM_RIGHT)
			img = shootOutput + STR_SHOOT + ARM_RIGHT + FILE_EXT_TIF;
		else if ('_' + name == STR_CLIP + ARM_RIGHT)
			img = shootOutput + STR_CLIP + ARM_RIGHT + FILE_EXT_TIF;
		else if ('_' + name == STR_SHOOT)
			img = shootOutput + STR_SHOOT + FILE_EXT_TIF;
		else if ('_' + name == STR_CLIP)
			img = shootOutput + STR_CLIP + FILE_EXT_TIF;
		else if (name.indexOf('background') == 0)
			return; // ignore background
		else if (name.indexOf('t') == 0)
			img = name.toUpperCase() + FILE_EXT_TIF;
		
		if (img == '')
		{
			LogError('PSB: layer ' + layer.name + " doesn't match any output");
			return;
		}
		
		img = outputPath + '/' + img;
		var imgFile = new File(img);
		if (!imgFile.exists)
		{
			LogError('PSB: layer ' + layer.name + ' output image not found ' + img);
			return;
		}
		
		Log('Opening ' + img);
		
		app.activeDocument.activeLayer = layer;
		layer_open_png(layer, imgFile);
	};
	
	this.resizeLayer = function(layer)
	{
		var name = layer.name.toLowerCase();
		
		//Resize layer
		if (this.needResize && (name == "arm left" || name == "arm right" || name == "body"  || name == "bust" 
			|| name == 'al' || name=='ar' || name == 'legs'
			|| name == 'lr' || name=='ll' || name == 'legs'))
		{
			app.activeDocument.activeLayer = layer;
			
			var test = getDBounds();
			//alert(test.width + ' ' + test.height);
			
			var x = layer.bounds[0].as('px');
			var y = layer.bounds[1].as('px');
			var x2 = layer.bounds[2].as('px');
			var y2 = layer.bounds[3].as('px');
			var w = x2 - x;
			var h = y2 - y;
			Log('BEFORE Resize layer ' + name + ' at ' + x + ' ' + y + ' to ' + x2 + ' ' + y2 + ' => ' + w + ' ' + h);
			
			layer.unlink();
			
			var fx = this.fx;//x * this.fx / 100;
			var fy = this.fy;// * this.fy / 100;
			
			//layer.resize(fx, fy, AnchorPosition.MIDDLECENTER);
			
			x = layer.bounds[0].as('px');
			y = layer.bounds[1].as('px');
			x2 = layer.bounds[2].as('px');
			y2 = layer.bounds[3].as('px');
			w = x2 - x;
			h = y2 - y;
			Log('AFTER Resize layer ' + name + ' at ' + x + ' ' + y + ' to ' + x2 + ' ' + y2 + ' => ' + w + ' ' + h);
			
			return;
		}
	}
	
}

function getDBounds(){
  var ref = new ActionReference();
  ref.putEnumerated( charIDToTypeID("Dcmn") , charIDToTypeID( "Ordn" ), charIDToTypeID( "Trgt" ) );
  desc1 = executeActionGet(ref);
  desc1Width = desc1.getUnitDoubleValue(stringIDToTypeID('width'));
  desc1Height = desc1.getUnitDoubleValue(stringIDToTypeID('height'));
  rObj = {width:desc1Width, height:desc1Height};
  return rObj;
} 