#include <iostream>
#include "tipl/tipl.hpp"

using namespace std;

#define TYPE_COREGISTRATION 0
#define TYPE_LDDMM 			1

#define ACTION_PREPARE 		0
#define ACTION_TRANSFORM 	1


//Main method (return 1 is success, errorCode otherwise)
int launch_process(int type, int action, string input1, string input2, string output);

//CO-REGISTRATION
int coreg_prepare(string body_origin, string body_target, string output);
int coreg_transform(string transform, string shooting, string output);

//LLDDMM
int lddmm_prepare(string body_origin, string body_target, string output);
int lddmm_transform(string transform, string shooting, string output);

//Generic load image
int load_bmp(tipl::image<float,3> & image, string path)
{
	if (!image.load_from_file<tipl::io::bitmap>(path.c_str()))
	{
		cout << "error loading " << path << endl;
		return 0;
	}
	return 1;
}
int load_bmp_2(tipl::image<float,2> & image, string path)
{
	if (!image.load_from_file<tipl::io::bitmap>(path.c_str()))
	{
		cout << "error loading " << path << endl;
		return 0;
	}
	return 1;
}

//Split
template<typename Out>
void split(const string &s, char delim, Out result) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        *(result++) = item;
    }
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, back_inserter(elems));
    return elems;
}

vector<string> read_file(string path)
{
	//Read transform txt
	ifstream file(path);
	stringstream buffer;
	buffer << file.rdbuf();
	string content = buffer.str();
	return split(content, '\n');
}

//Get filename from a path with or without extension
string get_filename(string filePath, bool withExtension = true, char seperator = '\\')
{
	// Get last dot position
	size_t dotPos = filePath.rfind('.');
	size_t sepPos = filePath.rfind(seperator);
 
	if(sepPos != string::npos)
		return filePath.substr(sepPos + 1,  (withExtension || dotPos == string::npos ? filePath.size() : dotPos) - sepPos - 1);
	return "";
}

//ENTRY POINT, expect 4 arguments : type action input1 input2
int main(int argc, char* argv[])
{
	if (argc < 6)
	{
		cout << "expect 4 or 5 arguments : type action input1 input2 output" << endl;
		return -1;
	}
	
	//Extract args
	int type = atoi(argv[1]);// 0=coregistration, 1=lddmm
	int action = atoi(argv[2]); //0=prepare, 1=transform
	string input1 = argv[3];
	string input2 = argv[4];
	string output = argv[5];
	
	int res = launch_process(type, action, input1, input2, output);
	if (res == 1)
		cout << "successful" << endl;
	else
		cout << "Error code : " << res << endl;
}

int launch_process(int type, int action, string input1, string input2, string output)
{
	//Launch process
	switch(type)
	{
		case TYPE_COREGISTRATION: 
			if (action == ACTION_PREPARE)
				return coreg_prepare(input1, input2, output);
			else if (action == ACTION_TRANSFORM)
				return coreg_transform(input1, input2, output);
			else
				return -3;
			break;
		case TYPE_LDDMM: 
			if (action == ACTION_PREPARE)
				return lddmm_prepare(input1, input2, output);
			else if (action == ACTION_TRANSFORM)
				return lddmm_transform(input1, input2, output);
			else
				return -3;
			break;
		default: return -2;
	}
	return 0;
}

//Prepare co-registration
int coreg_prepare(string body_origin, string body_target, string output)
{
    tipl::image<float,3> origin, target, output_image;

	//Load inputs
	if (!load_bmp(origin, body_origin) || !load_bmp(target, body_target))
		return 0;

	//Outputs
	string transTxtPath = output + ".txt";
	string resultBmpPath = output + ".bmp";
	
	cout << "Coreg: Preparing transform : " << output << endl;
	
    bool terminated = false;
	bool withRandom = true; // if false, no transformation at all...hmm
	tipl::vector<3, float> from_vs, to_vs; // vector scales
	from_vs[0] = from_vs[1] = from_vs[2] = 1.0f;
	to_vs[0] = to_vs[1] = to_vs[2] = 1.0f;
    tipl::affine_transform<float> T; // transformation to compute
	tipl::reg::reg_type transformType =  tipl::reg::affine;//translocation,rigid_body,rigid_scaling,affine
	//Apply co-registration (NOT WORKING WELL...)
    tipl::reg::linear(origin,from_vs,target,to_vs,T, transformType,tipl::reg::mutual_information(),withRandom,terminated,0.005);
		
	//TEST
	//T.translocation[0] = T.translocation[1] = T.translocation[2] = 0; // replace dumb values
	//T.translocation[2] = 0;
	//T.rotation[0] = T.rotation[1] = T.rotation[2] = 0; // replace dumb values
	//T.affine[0] = T.affine[1] = T.affine[2] = 0;
	//T.scaling[2] = T.affine[2] = 0;
	//T.scaling[2] = 1; // we don't want a z scale
	//T.scaling[0] = T.scaling[1] = 1.06; // test to confirm that scale is inverted... WTF
	
	//Save transformation matrix
	cout << "Saving transform : " << transTxtPath << endl;
	ofstream transfFile(transTxtPath);
	for (int i = 0; i < 3; ++i)
		transfFile << T.translocation[i] << " ";
	transfFile << endl;
	for (int i = 0; i < 3; ++i)
		transfFile << T.rotation[i] << " ";
	transfFile << endl;
	for (int i = 0; i < 3; ++i)
		transfFile << T.scaling[i] << " ";
	transfFile << endl;
	for (int i = 0; i < 3; ++i)
		transfFile << T.affine[i] << " ";
    transfFile.close();
	
	//Conversion affine_transform to transformation_matrix
    tipl::transformation_matrix<float> result(T, origin.geometry(), from_vs, target.geometry(), to_vs);
    //tipl::reg::shift_to_center(origin.geometry(),target.geometry(),result); // this function does NOT exists LOL
	
	//Apply transformation
    output_image.resize(origin.geometry());
    tipl::resample(target,output_image,result, tipl::cubic);
	
	//Save result image
	cout << "Coreg: Saving image : " << resultBmpPath << endl;
	remove(resultBmpPath.c_str());
    output_image.save_to_file<tipl::io::bitmap>(resultBmpPath.c_str());
	
	return 1;	
}

//Apply co-registration transformation
int coreg_transform(string transform, string shooting, string output)
{
	tipl::image<float,3> image, output_image;
	
	//Load bmp
    if (!load_bmp(image, shooting))
		return 0;
	
	string outputBmp = output + ".bmp";
	
	//Read transform txt
	vector<string> lines = read_file(transform);
	
	//Load affine transform
	tipl::affine_transform<float> T;
	int i = 0;
	for (auto it = lines.begin(); it != lines.end(); ++it, ++i)
	{
		float * array;
		if (i == 0)
			array = T.translocation;
		else if (i == 1)
			array = T.rotation;
		else if (i == 2)
			array = T.scaling;
		else if (i == 3)
			array = T.affine;
		else 
			break; // only 4 lines to read
		
		vector<string> vals = split(*it, ' ');
		int j = 0;
		for (auto itVal = vals.begin(); itVal != vals.end(); ++itVal, ++j)
		{
			float val = atof(itVal->c_str());
			array[j] = val;
		}
	}
		
	//Set matrix
	tipl::vector<3, float> vs; vs[0] = vs[1] = vs[2] = 1;
	tipl::transformation_matrix<float> result(T, image.geometry(), vs, image.geometry(), vs);

	//Apply transformation
	output_image.resize(image.geometry());
	tipl::resample(image,output_image,result, tipl::cubic);
	output_image.save_to_file<tipl::io::bitmap>(outputBmp.c_str());	
	
	return 1;	
}

//LDDMM
int lddmm_prepare(string body_origin, string body_target, string output)
{
    tipl::image<float,2> I0,I1;

	//Load bmp
    if (!load_bmp_2(I0, body_origin) || !load_bmp_2(I1, body_target))
		return 0;
	
    // The fft used in lddmm requires the image size to be 2^n
    {
        tipl::vector<2,int> from,to;
        tipl::fft_round_up(I0,from,to);
        tipl::fft_round_up(I1,from,to);
    }


    tipl::image<float,2> J0,J1;
    // the resulted deformed image of each time frame

    tipl::image<tipl::vector<2>,2> s0,s1;
    // the resulted deformation metric of each time frame

	//Outputs
	string transTxtPath = output + ".txt";
	string resultBmpPath = output + ".bmp";
	cout << "LDDMM: preparing transform " << output << endl;
	
    // perform LDDMM
	tipl::reg::fast_lddmm(I0,I1,J0,J1,s0,s1);
	
	//TODO : save transformation
	tipl::image<tipl::vector<2>,2> transf = s0;//[0];
	
	//Save transformation matrix
	cout << "Saving transform : " << transTxtPath << endl;
	ofstream transfFile(transTxtPath);
	int i = 0;
	for(tipl::pixel_index<2> pos(s0.geometry());i < s0.size();++i, ++pos)
	{
		//int x = pos[0];
		//int y = pos[1];
		//transfFile << x << " " << y << endl;
		tipl::vector<2> val = s0[pos.index()];
		transfFile << val[0] << " " << val[1] << endl;
	}
    transfFile.close();
	
	//Save bmp
	J0.save_to_file<tipl::io::bitmap>((output+"_0.bmp").c_str());
	J1.save_to_file<tipl::io::bitmap>((output+"_1.bmp").c_str());

	return 1;
	
    //tipl::reg::lddmm(I0,I1,J0,J1,s0,s1,20); // T=1
	
	// Store the process as bitmap files
/*
    tipl::image<float,2> JOut(tipl::geometry<2>(J0[0].width() << 1,J0[0].height()));
    for(size_t index = 0; index < J0.size(); ++index)
    {
        //tipl::draw(J0[index],JOut,tipl::pixel_index<2>());
        tipl::draw(J1[index],JOut,tipl::pixel_index<2>((int)J0[index].width(),(int)0,JOut.geometry()));
        ostringstream out1,out2;
        out1 << "result_lddmm_" << index << ".bmp";
		
		cout << "LDDMM: saving transform " << out1.str() << endl;

        JOut.save_to_file<tipl::io::bitmap>(out1.str().c_str());
    }
	*/
	return 1;	
}

int lddmm_transform(string transform, string shooting, string output)
{
	tipl::image<float,2> image;
	tipl::image<float,2> outputImage;
	
	//Load bmp
    if(!load_bmp_2(image, shooting))
        return 0;
    
	//Round up for lddmm
	{	
        tipl::vector<2,int> from,to;
        tipl::fft_round_up(image,from,to);
    }
	
	//Read transform txt
	vector<string> lines = read_file(transform);
	tipl::image<tipl::vector<2>,2> transf;
	transf.resize(image.geometry());
	int i = 0;
	tipl::pixel_index<2> pos(transf.geometry());
	cout << "Reading transformation: " << lines.size() << " lines " << endl;
	for (auto it = lines.begin(); it != lines.end(); ++it, ++pos)
	{
		vector<string> vals = split(*it, ' ');
		if (vals.size() < 2) continue;
		transf[pos.index()][0] = atof(vals[0].c_str());
		transf[pos.index()][1] = atof(vals[1].c_str());
	}
	
	//Apply transformation
	cout << "Applying..." << endl;
	outputImage = image;
	tipl::compose_mapping(image, transf, outputImage);
	
	//Save bmp
	outputImage.save_to_file<tipl::io::bitmap>((output+".bmp").c_str());
	
	return 1;	
}
