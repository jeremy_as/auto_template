///////////////////////////////////////////////////////////////////////////////
// LIBS
///////////////////////////////////////////////////////////////////////////////

#include "C:/LOCAL_COMPOSITOR/lib/_includes.jsx"

#include "C:/LOCAL_COMPOSITOR/MultiShape/PuppetTransform.jsx"
#include "C:/LOCAL_COMPOSITOR/MultiShape/MultiShape.jsx"


///////////////////////////////////////////////////////////////////////////////
// MAIN 
///////////////////////////////////////////////////////////////////////////////
function main()
{
	version_check();
	
	//Remember ruler units; switch to pixels
	var originalRulerUnits = preferences.rulerUnits;
	var originalDisplayDialogs = app.displayDialogs;
	preferences.rulerUnits = Units.PIXELS;
	app.displayDialogs = DialogModes.NO;
		
	//Set current folder (directory of launch)
	Folder.current = $.fileName.substring(0, $.fileName.lastIndexOf('/')) + '/';
	
	//Avoid mislaunch from server
	if (new File('PuppetTransform.jsx').exists)
	{
		alert('COPY MULTISHAPE.bat IN A LOCAL WORKING DIRECTORY');
		return;
	}

	//Init log
	Log('launch', true);
	LogError('', true);

	//Create Multishape instance
	var multishape = new MultiShape();

	//Let's go
	var allOk = multishape.start();
	
	//Restore original ruler unit
	preferences.rulerUnits = originalRulerUnits;
	app.displayDialogs = originalDisplayDialogs;
		
	//All done
	var msgError = !allOk ? ' with errors : see error.log' : ' without errors';
	Log('done' + msgError);
	Alert('done' + msgError);
	
	//Show log
	if (!allOk)
	{
		if (confirm('Do you want to open error log?'))
			LogErrorOpen();
	}
	else if (confirm('Do you want to open output log?'))
		LogOpen();
}


///////////////////////////////////////////////////////////////////////////////
// ERROR HANDLING
///////////////////////////////////////////////////////////////////////////////
function handleError(e)
{
	// don't report error on user cancel
	if (e.number == 8007) return;
		
	if (confirm('An unknown error has occurred.\n' + 'Would you like to see more information?', true, 'Unknown Error'))
		alert(e + ': on line ' + e.line, 'Script Error', true);
}

///////////////////////////////////////////////////////////////////////////////
// MAIN CALL
///////////////////////////////////////////////////////////////////////////////
try { main() } catch (e) { handleError(e) }