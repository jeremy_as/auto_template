set COMPOSITOR_PATH=\\superman\RD\2D\03_RD\_SCRIPT_TEST
set PHOTOSHOP_PATH=C:\Program Files\Adobe\Adobe Photoshop CC 2018

REM IMPORT THE GOOD VERSION OF THE COMPOSITOR
IF NOT EXIST "C:\LOCAL_COMPOSITOR\" (
mkdir "C:\LOCAL_COMPOSITOR"
)
robocopy %COMPOSITOR_PATH% C:\LOCAL_COMPOSITOR\ /MIR

set CURR_PATH=%~dp0
set CURR_PATH=%CURR_PATH:~0,-1%
if NOT exist config_face.xml (
	REM FIRST LAUNCH
	move C:\LOCAL_COMPOSITOR\config_face.xml "%CURR_PATH%\config_face.xml"
) else (
	REM LAUNCH COMPOSITOR FROM CURRENT DIR
	copy C:\LOCAL_COMPOSITOR\main_face.jsx "%CURR_PATH%\main_face.jsx"
	"%PHOTOSHOP_PATH%\Photoshop.exe" "%CURR_PATH%\main_face.jsx"
	
	timeout 1
	del "%CURR_PATH%\main_face.jsx"
)