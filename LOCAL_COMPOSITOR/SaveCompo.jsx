///////////////////////////////////////////////////////////////////////////////
// LIBS
///////////////////////////////////////////////////////////////////////////////

#include "C:/LOCAL_COMPOSITOR/lib/_includes.jsx"


///////////////////////////////////////////////////////////////////////////////
// MAIN 
///////////////////////////////////////////////////////////////////////////////
function main()
{
	version_check();
	
	//Init log
	Log('', true);
	LogError('', true);
	
	if (!app.activeDocument)
	{
		alert('no document active to save');
		return false;
	}
	
	//Set current folder (directory of launch)
	Folder.current = $.fileName.substring(0, $.fileName.lastIndexOf('/')) + '/';
	
	//Check psb file
	var files = file_get_all(Folder.current, "*" + FILE_EXT_PSB);
	if (files.length === 0)
	{
		alert (FILE_EXT_PSB + ' file not found in ' + Folder.current);
		return;
	}
	
	var psbFile = files[0];
	var file = new File(psbFile.fullName.replace(FILE_EXT_PSB, FILE_EXT_XML));
	
	if (!confirm("Your active document will be saved in " + file.fsName + "\ncontinue?"))
		return;
	
	//Duplicate doc
	app.activeDocument.duplicate('mother_empty');
	
	//Save it	
	var ok = save_mother_compo(file);
	
	//Close duplicated doc
	close_document();
	
	if (ok)
		alert('Compo saved successfully : ' + file.fsName);
	else if (confirm('An error occured. Do you want to open error log?'))
		LogErrorOpen();
}

///////////////////////////////////////////////////////////////////////////////
// ERROR HANDLING
///////////////////////////////////////////////////////////////////////////////
function handleError(e)
{
	// don't report error on user cancel
	if (e.number == 8007) return;
		
	if (confirm('An unknown error has occurred.\n' + 'Would you like to see more information?', true, 'Unknown Error'))
		alert(e + ': on line ' + e.line, 'Script Error', true);
}

///////////////////////////////////////////////////////////////////////////////
// MAIN CALL
///////////////////////////////////////////////////////////////////////////////
try { main() } catch (e) { handleError(e) }