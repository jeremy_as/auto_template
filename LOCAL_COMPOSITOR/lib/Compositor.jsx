﻿///////////////////////////////////////////////////////////////////////////////
// DEFINES
///////////////////////////////////////////////////////////////////////////////

//XML conf file (keep track of files dates)
var CONF_FILE = 'config.xml';
var CONF_FILE_FACE = 'config_face.xml';

//Action to load
var ACTION_FILE_NAME = 'AS_TOOLS'; // TODO : attribute in xml?
var ACTION_COMPO = 'AS_COMPO_';
var ACTION_RASTERIZE = 'RASTERIZE ALL LAYERS';


// Mask File
var ARM_LEFT_MASK = 'ARM_L_MASK';
var ARM_RIGHT_MASK = 'ARM_R_MASK';


//Render path folders
var RENDER_BODY_PARTS = 'BODY_PARTS';
var RENDER_SHADOWS = 'SHADOWS';
var RENDER_FULL = 'FULL';

//Collection path folders
var COLLECTION_SUBDIRS = ['01_SPEC_COMPO','02_TEMPLATE'];
var COLLECTION_SUBDIR_COMPO = 0;
var COLLECTION_SUBDIR_TEMPLATE = 1;
var COLLECTION_MOTHER_COMPO = '_MOTHER_COMPO';

var COLLECTION_SETTINGS = '_SETTINGS'; // settings folder, contains .atn and mother compo by skin
var COLLECTION_TEMPLATE = '_TEMPLATE'; // default template architecture (in _settings dir)

//Local folder (working dir)
var LOCAL_COMPOSITOR = 'C:/LOCAL_COMPOSITOR/';
var COMPO_INPUT = LOCAL_COMPOSITOR + 'input';
var COMPO_BAT_OPEN = 'open_compo.bat';
var COMPO_BAT_SAVE = 'save_compo.bat';
var EMPTY_PSB = 'empty/_empty.psb';

//Final template files
var TEMPLATE_PARTS = ['ARM_L', 'ARM_R', 'LEG_L', 'LEG_R', 'BUST', 'FACE'
						, 'BACKGROUND','ARM_L_MASK', 'ARM_R_MASK', 'BUST_SH_FROM_ARM_L', 'BUST_SH_FROM_ARM_R', 'FLOOR_SH']; //TODO

//Data model hierarchy (Skin/Pose/Variation/Angle) + shadows (variation/angle in pose)
var DATA_MODEL = ['skin', 'pose', 'variation', 'angle', 'shadow', 'angle'];
var DATA_SKIN = 0;
var DATA_POSE = 1;
var DATA_VARIATION = 2;
var DATA_ANGLE = 3;
var DATA_SHADOW = 4;
var DATA_SHADOW_ANGLE = 5;

///////////////////////////////////////////////////////////////////////////////
// CLASSES
///////////////////////////////////////////////////////////////////////////////

//Data folder class
function DataFolder(name, date, parent)
{
	this.name = name;
	
	this.date = date;
	this.isNew = false;
	
	this.parent = parent;
	this.children = {}; // indexed by name
	
	this._path = '';
	
	//Hierarchy
	if (parent)
		parent.children[name] = this;
	
	//Return path (/skinName/poseName/variationName/angleName) -> starts with /
	this.getPath = function()
	{
		//Return path cached
		if (this._path != '') return this._path;
		
		var folder = '';
		if (this.parent)
			folder = this.parent.getPath() + '/';
		
		//Store path in cache
		this._path = folder + this.name;
		return this._path;
	}
	
	this.hasChild = function(name)
	{
		return this.children.hasOwnProperty(name);
	}
	
	this.getFirstChild = function()
	{
		for (var i in this.children)
			return this.children[i];
		return null;
	}
	
	//Only for pose
	this.shadows = {};
	this.hasShadow = function(shadowName)
	{
		return this.shadows.hasOwnProperty(shadowName);
	}
	this.getFirstShadow = function(type)
	{
		var shadowList = {'ARM': 'AS', 'BUST': 'BUST', 'FLOOR': 'FF'};
		var search = shadowList[type];
		for (var i in this.shadows)
		{
			//alert(this.shadows[i].name);
			if (this.shadows[i].name.indexOf(search) == 0)
				return this.shadows[i];
		}
		return null;
	}
	
}

//COMPOSITOR (MAIN CLASS)
//This class read CONF_FILE and keep collectionPath sync with renderPath
function Compositor()
{
	this.xmlDoc = null;
	
	//3D Render path (which contains subdirectories skins/poses/variations)
	this.renderPath = '';
	
	//2D Collection path (which contains subdirectories COLLECTION_SUBDIRS)
	this.collectionPath = '';
    
    
    //2D Collection path (which contains subdirectories COLLECTION_SUBDIRS)
	this.templatePath = '';
	
    //2D Collection path (which contains subdirectories COLLECTION_SUBDIRS)
	this.workingPath = '';
    
	//Top DataFolder
	this.root = new DataFolder('', new Date(0), null);
	
	//Fatal errors
	this.errorCreateDir = false;
	this.errorMissingFile = false;
	this.errorCompo = false;
	this.warningRenderFile = false; // warning if something missing
	
	//Read xml and compare with renderpath to execute needed update, and save xml with date updated
	this.start = function()
	{
		//Reset members
		this.errorCreateDir = false;
		this.errorMissingFile = false;
		this.errorCompo = false;
		this.warningRenderFile = false;
		this.root = new DataFolder('', new Date(0), null);
		
		//Load xml from collection path
		if (!this.loadXml())
			return false; // something went wrong, see error.log for details
		
		//Read render path and update collection folders
		if (!this.readRenderPath())
			return false; // something went wrong, see error.log for details
			
		//Check warning
		if (this.warningRenderFile)
		{
			if (!confirm('Something missing in render files, do you want to continue?'))
				return false;
		}
		
		//Process necessary composition locally, and upload to collection path


        this.processCompositions();
			
		//Save new config xml and update config xml in collection path
		this.saveXml();

		return !this.errorMissingFile && !this.errorCompo;
	};


    this.start_face_integration = function()
	{
		//Reset members
		this.errorCreateDir = false;
		this.errorMissingFile = false;
		this.errorCompo = false;
		this.warningRenderFile = false;
		this.root = new DataFolder('', new Date(0), null);
		
		//Load xml from collection path
		if (!this.loadXmlFace())
			return false; // something went wrong, see error.log for details
		
		return this.processFace();
	
	};
	
	this.formatDuration = function(duration)
	{
		var h = parseInt(duration / 3600000);
		var left = duration - h * 3600000;
		var m = parseInt(left / 60000);
		left -= m * 60000;
		var s = parseFloat(left / 1000).toFixed(2);
		return (h > 0 ? h + ' h ' : '') + (m > 0 ? m + ' m ' : '') + s + ' s';
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	// XML
	///////////////////////////////////////////////////////////////////////////////
	
	//Read data model
	this.loadXml = function()
	{
		//Read local config
		var config = new File(CONF_FILE);
		if (!config.open('r'))
		{
			LogError('FATAL ERROR OPENING LOCAL CONF FILE ' + CONF_FILE);
			return false;
		}
		var content = config.read();
		config.close();
		this.xmlDoc = new XML(content);
		
		//Read path attributes
		this.collectionPath = path_format(this.xmlDoc.@collectionpath, true);
		
		//Get last config file from collection path
		config = new File(this.collectionPath + CONF_FILE);
		if (config.exists)
		{
			if (!config.open('r'))
			{
				LogError('FATAL ERROR OPENING CONF FILE IN COLLECTION PATH ' + this.collectionPath + CONF_FILE);
				return false;
			}
			content = config.read();
			config.close();
			this.xmlDoc = new XML(content);
		}
		
		//Read last attributes
		this.renderPath = path_format(this.xmlDoc.@renderpath, true);
		this.root.date = new Date(this.xmlDoc.@date);
		
		//Read xml folders
		this.loadXmlNode(this.root, this.xmlDoc, 0);
		return true;
	};

    //Read data model
	this.loadXmlFace = function()
	{
		//Read local config
		var config = new File(CONF_FILE_FACE);
		if (!config.open('r'))
		{
			LogError('FATAL ERROR OPENING LOCAL CONF FILE ' + CONF_FILE_FACE);
			return false;
		}
		var content = config.read();
		config.close();
		this.xmlDoc = new XML(content);
		
		//Read path attributes
        this.templatePath = path_format(this.xmlDoc.@templatepath, true);
        this.workingPath = path_format(this.xmlDoc.@workingpath, true);
        return true;
	};
	
	//Recursive method to read xml folders hierarchy
	this.loadXmlNode = function(parent, parentNode, dataIndex)
	{
		var tagName = DATA_MODEL[dataIndex];
		
		//Read shadows
		if (dataIndex == DATA_VARIATION)
		{
			this.loadXmlNode(parent, parentNode, DATA_SHADOW);
		}
		
		//Read children
		var length = parentNode[tagName] ? parentNode[tagName].length() : 0;
		for (var i = 0; i < length; i++)
		{
			var elemNode = parentNode[tagName][i];
			var elemName = elemNode.@name;
			var elemDate = new Date(elemNode.@date);
			var elem;
			if (dataIndex == DATA_SHADOW)
			{
				elem = new DataFolder(elemName, elemDate, null);
				parent.shadows[elemName] = elem;
				elem.parent = parent;
			}
			else
				elem = new DataFolder(elemName, elemDate, parent);

			//TMP debug to construct file system from xml instead of renderpath
			//this.onNewFolder(elem, dataIndex);
			
			//Recursive call for child hierarchy
			if (dataIndex < DATA_MODEL.length)
				this.loadXmlNode(elem, elemNode, dataIndex + 1);
        }
	};
	
	//Save data model (hierarchy of datafolder)
	//TODO : return true or false, be SAFE ! confirm while not saved ?
	this.saveXml = function()
	{
        //Delete all skins (top hierarchy)
        delete this.xmlDoc[DATA_MODEL[0]];

        //Re add them (to keep track of deleted elements -> xml lib sucks)
        for (var name in this.root.children)
        {
            var skin = this.root.children[name];
            this.saveXmlNode(skin, this.xmlDoc, 0);
        }
		
		//Save root attributes
		this.xmlDoc.@renderpath = this.renderPath;
		this.xmlDoc.@collectionpath = this.collectionPath;
		this.xmlDoc.@date = this.root.date;

		//Save file locally
        var config = new File(CONF_FILE);
		config.open('w');
		config.write(this.xmlDoc.toXMLString());
		config.close();
		
		//Copy file to collection path
		config.copy(this.collectionPath + CONF_FILE);
	};

	//Recursive method to save a datafolder into an xml node
    this.saveXmlNode = function(elem, parentNode, dataIndex)
    {
        var tagName = DATA_MODEL[dataIndex];
		
		//Create node & add attributes
        var node = xml_create_node(parentNode, tagName);
		node.@name = elem.name;
        node.@date = elem.date;
		
		//Shadows
		if (dataIndex == DATA_POSE)
		{
			for (var name in elem.shadows)
			{
				var shadow = elem.shadows[name];
				this.saveXmlNode(shadow, node, DATA_SHADOW);
			}
		}

		//Recursive call for child hierarchy
        for (var name in elem.children)
        {
            var child = elem.children[name];
            this.saveXmlNode(child, node, dataIndex + 1);
        }
    };
	
	
	///////////////////////////////////////////////////////////////////////////////
	// RENDER FILES (FROM 3D)
	///////////////////////////////////////////////////////////////////////////////

	//Read render path hierarchy
	this.readRenderPath = function()
	{
		//Read models
		this.readFolder(this.root, this.renderPath, 0);
		
		//Check error
		if (this.errorCreateDir)
		{
			LogError('FATAL ERROR CREATE DIR');
			return false;
		}
		
		return true;
	};

	//Read folder recursively
	this.readFolder = function(parent, currPath, dataIndex)
	{
		//Variation -> read variations in body parts
		if (dataIndex == DATA_VARIATION)
		{
			this.readVariations(parent, currPath);
			return;
		}

        //Angle -> read render files in ID/PL/SK
		if (dataIndex == DATA_ANGLE)
		{
			this.readRenderFiles(currPath, parent, false);
			return;
		}
		
		//Other -> go down
		this.readSubFolders(parent, currPath, dataIndex, true);
	}
	
	this.readSubFolders = function(parent, currPath, dataIndex, withDelete, dirName)
	{		
		var typeName = DATA_MODEL[dataIndex];
		Log('reading ' + typeName + 's in ' + currPath);
		
		//Get subfolders
		var dirs = dir_get_all(currPath);
		if (withDelete)
			var dirName = {};
		
		for (var i = 0; i < dirs.length; i++)
		{
			var dir = dirs[i];
			var child = null;
			
            dirName[dir.name] = 1; // keep track of dir found for delete

			var child = this.onFolderFound(parent, dataIndex, dir.name, dir.modified);
			
			//Read next folder recursively
			this.readFolder(child, currPath + dir.name + '/', dataIndex + 1);
		}

		if (withDelete)
			this.checkFoldersDeleted(parent, dirName, dataIndex);
	};
	
	this.readVariations = function(pose, path)
	{
		//Get sub folders in body parts
		var dirs = dir_get_all(path + RENDER_BODY_PARTS);
        var dirName = {};
		Log('reading body parts in ' + path + RENDER_BODY_PARTS);
		
		//Read variations in sub folders
		for (var i = 0; i < dirs.length; i++)
		{
			var dir = dirs[i];
			if (dir.name == RENDER_FULL) // ignore full (not a variation)
				continue;
			
			this.readSubFolders(pose, path + RENDER_BODY_PARTS + '/' + dir.name + '/', DATA_VARIATION, false, dirName);
		}

		//Delete not found
		this.checkFoldersDeleted(pose, dirName, DATA_VARIATION);
		
		//Parse shadows 
		this.readShadows(pose, path + RENDER_SHADOWS);
	}
	
	this.readShadows = function(pose, path)
	{
		var dirs = dir_get_all(path);
        var dirNames = {};
		Log('reading all shadows subdirs in ' + path);
		//Read shadows in sub folders
		for (var i = 0; i < dirs.length; i++)
		{
			var dir = dirs[i];
			this.readShadowsOn(pose, path + '/' + dir.name, dir.name, dirNames);
		}
		
		this.checkFoldersDeleted(pose, dirNames, DATA_SHADOW);
	}
	
	this.readShadowsOn = function(pose, path, subdir, dirNames)
	{
		var dirs = dir_get_all(path);
        Log('reading shadows in ' + path);
		//Read shadows in sub folders
		for (var i = 0; i < dirs.length; i++)
		{
			var dir = dirs[i];
			var onVariation = '';
			var fromVariation = '';
			if (subdir == 'ARMS')
				fromVariation = dir.name;
			else if (subdir == 'FLOOR')
			{
				dirNames[dir.name] = 1;
				this.readShadowsFiles(pose, path + '/' + dir.name, dir.modified, dir.name);
				continue;
			}
			else
				onVariation = dir.name;
			
			this.readShadowsFrom(pose, path + '/' + dir.name, subdir, onVariation, fromVariation, dirNames);
		}
	}
	
	this.readShadowsFrom = function(pose, path, subdir, onVariation, fromVariation, dirNames)
	{
		var dirs = dir_get_all(path);
		Log('reading shadows from in ' + path);
		//Read shadows in sub folders
		for (var i = 0; i < dirs.length; i++)
		{
			var dir = dirs[i];
			
			if (subdir == 'ARMS')
				onVariation = dir.name;
			else
				fromVariation = dir.name;
			
			var shadowName = onVariation + '_' + fromVariation;
			dirNames[shadowName] = 1;
			this.readShadowsFiles(pose, path + '/' + dir.name, dir.modified, shadowName);
		}
	}

	this.readShadowsFiles = function(pose, path, date, shadowName)
	{
		var shadow = null;
		if (pose.hasShadow(shadowName))
		{
			shadow = pose.shadows[shadowName];
			//shadow file updated
			if (date > shadow.date)
			{
				//Store new date
				shadow.date = date;
				shadow.isNew = true;
				Log('shadow updated : ' + shadowName);
			}
		}
		//New shadow found
		else
		{
			shadow = new DataFolder(shadowName, date, null);
			shadow.parent = pose;
			pose.shadows[shadowName] = shadow;

			shadow.isNew = true;
			Log('new shadow found : ' + shadowName);
		}
		
		Log('reading ' + shadowName + ' angles in ' + path);
		
		//L & R
		var dirName = {};
		if (path.indexOf('/FLOOR/') >= 0)
			this.readAnglesInPath(shadow, path, dirName, DATA_SHADOW_ANGLE);
		else
		{
			this.readAnglesInPath(shadow, path + '/L', dirName, DATA_SHADOW_ANGLE);
			this.readAnglesInPath(shadow, path + '/R', dirName, DATA_SHADOW_ANGLE);
		}
		
		this.checkFoldersDeleted(shadow, dirName, DATA_SHADOW_ANGLE);
	}
	
	//Read render files (angles of a variation)
	this.readRenderFiles = function(currPath, variation, inSubdir, dirNames)
	{
		var dirName = {};
		
		//Legs or feet
		if (!inSubdir && (currPath.indexOf('LEGS') >= 0 || currPath.indexOf('ARMS') >= 0))
		{
			//Subdirs L and R
			this.readRenderFiles(currPath + 'L/', variation, true, dirName);
			this.readRenderFiles(currPath + 'R/', variation, true, dirName);
			this.checkFoldersDeleted(variation, dirName, DATA_ANGLE);
			return;
		}
		else if (inSubdir)
			dirName = dirNames; // keep same list
		
        //Read render files in SK
		this.readAnglesInPath(variation, currPath + 'SK', dirName);
		
		//Read ids
		this.readAnglesInPath(variation, currPath + 'ID/ID_Solo' , dirName);
		
		//TODO : PL ?? pilosity : maybe later
				
		//Check deleted
		if (!inSubdir)
			this.checkFoldersDeleted(variation, dirName, DATA_ANGLE);
	};
	
	this.readAnglesInPath = function(variation, path, dirName, dataIndex)
	{
		if (typeof(dataIndex) == 'undefined')
			dataIndex = DATA_ANGLE;
		var dir = new Folder(path);
		if (!dir.exists)
		{
			this.warningRenderFile = true;
			LogError('missing dir ' + path);
			return;
		}
		
		var files = file_get_all(path, '*' + FILE_EXT_PNG);
		if (files.length == 0)
		{
			this.warningRenderFile = true;
			LogError('no files found in ' + path);
			return;
		}
		
		Log('reading ' + files.length + ' render files in ' + path);
		for (var i = 0; i < files.length; i++)
		{
			var file = files[i];
			
			//Extract angle
			var name = file.name;
			var angleName = name.substring(name.length-8, name.length-4);
			var angle = this.onFolderFound(variation, dataIndex, angleName, file.modified);
			
			dirName[angleName] = 1;
		}
	}
		
	//Called when a 'dataFolder' is parsed (skin/pose/variation/angle)
	this.onFolderFound = function(parent, dataIndex, name, date)
	{
		var child = null;
		
		//Existing child
		if (parent.hasChild(name))
		{
			child = parent.children[name];
			//child file updated
			if (date > child.date)
			{
				//Store new date
				child.date = date;
				child.isNew = true;
			}
		}
		//New child found
		else
		{
			child = new DataFolder(name, date, parent);
			this.onNewFolder(child, dataIndex);

			child.isNew = true;
		}
		
		return child;
	}
	
	//Used when parsing to ask to delete removed 'dataFolders'
	this.checkFoldersDeleted = function(parent, dirName, dataIndex)
	{
		var typeName = DATA_MODEL[dataIndex];
		var children = parent.children;
		if (dataIndex == DATA_SHADOW)
			children = parent.shadows;
		
        //Check deleted folders
        var foldersToDelete = [];
        for (var childName in children)
        {
            var child = children[childName];
            if (!dirName.hasOwnProperty(childName))
            {
                var path = child.getPath();
                var msg = typeName + ' removed : ' + path;

				//Ask for confirm
                if (confirm(msg + '\nDo you want to delete it?'))
                {
                    foldersToDelete.push(child);
                    msg += ' -> deleted';
                }
                else
                    msg += ' -> keep files';

                Log(msg);
            }
        }
		
		//Remove all confirmed
        for (var i = 0; i < foldersToDelete.length; i++)
        {
            var folder = foldersToDelete[i];
            delete children[folder.name];
            this.onFolderDeleted(folder, dataIndex);
            Log(typeName + ' ' + folder.name + ' deleted');
        }
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// COLLECTION FOLDERS
	///////////////////////////////////////////////////////////////////////////////
	
	//Called when a new folder is found on render path -> Create folders in collection path if not exist
	this.onNewFolder = function(folder, dataIndex)
	{
		Log('new ' + DATA_MODEL[dataIndex] + ' detected: ' + folder.name);
		
		if (dataIndex >= DATA_SHADOW)
			return;
		
		if (dataIndex == DATA_VARIATION)
			return; // ignore variations (will be files, not folders)
		
		var folderPath = folder.getPath();
		
		//angle
		if (dataIndex == DATA_ANGLE)
			folderPath = '/' + folder.parent.parent.parent.name + '/' + folder.parent.parent.name + '/' + folder.name;
		
		for (var i = 0; i < COLLECTION_SUBDIRS.length; i++)
		{
			var collectionDirPath = this.collectionPath + COLLECTION_SUBDIRS[i];
			
			this.createDir(collectionDirPath + folderPath);
			if (dataIndex == DATA_ANGLE)
			{
				if (i == COLLECTION_SUBDIR_COMPO)
					this.createSpecCompo(folderPath);
			}
		}
		
		//Top hierarchy (skin)
		if (dataIndex == DATA_SKIN)
		{
			//Create dir in settings too
			var destPath = this.collectionPath + COLLECTION_SETTINGS + folderPath + '/';
			var dirExists = new Folder(destPath).exists;

			if (!dirExists)
			{
				var ok = this.createDir(destPath);
			
				//Copy .bat files for mother compo
				if (ok)
				{
					var motherName = folderPath.substring(1).replace(/\//g, '_');
					
					Log('creating mother compo ' + motherName);
					
					//Init mother
					this.initMotherDir(destPath, motherName);
					
					//Init shadows
					this.createDir(destPath + 'SH_ARM/');
					this.initMotherDir(destPath + 'SH_ARM/', motherName + '_SH_ARM');
					this.createDir(destPath + 'SH_BUST/');
					this.initMotherDir(destPath + 'SH_BUST/', motherName + '_SH_BUST');
					this.createDir(destPath + 'SH_FLOOR/');
					this.initMotherDir(destPath + 'SH_FLOOR/', motherName + '_SH_FLOOR');
				}
			}
			
			//Default template dir
			this.createDir(this.collectionPath + COLLECTION_SETTINGS + '/' + COLLECTION_TEMPLATE);
		}
	}
	
	this.initMotherDir = function(destPath, motherName)
	{
		Log('init mother dir ' + destPath + motherName);
		
		//Prepare save.bat
		new File(LOCAL_COMPOSITOR + COMPO_BAT_SAVE).copy(destPath + COMPO_BAT_SAVE);
		//Create empty file with the good name
		new File(LOCAL_COMPOSITOR + EMPTY_PSB).copy(destPath + motherName + COLLECTION_MOTHER_COMPO + FILE_EXT_PSB);
	}
	
	this.createDir = function(path)
	{
		var ok = dir_create(path);
		if (!ok)
		{
			LogError('error creating path ' + path);
			this.errorCreateDir = true;
		}
		return ok;
	}

	this.createSpecCompo = function(folderPath)
	{
		var destPath = this.collectionPath + COLLECTION_SUBDIRS[COLLECTION_SUBDIR_COMPO] + '/';
		var destFile = destPath + folderPath + '/' + folderPath.substring(1).replace(/\//g, '_') + COLLECTION_MOTHER_COMPO + FILE_EXT_PSB;
		if (new File(destFile).exists)
			return;
		
		Log("creating spec compo for " + folderPath);
		
		//Copy save_compo.bat file
		new File(LOCAL_COMPOSITOR + COMPO_BAT_SAVE).copy(destPath + folderPath + '/' + COMPO_BAT_SAVE);
		
		//Create empty file with the good name
		new File(LOCAL_COMPOSITOR + EMPTY_PSB).copy(destFile);		
	}
		
	//Call when user choose to delete removed 'dataFolders'
    this.onFolderDeleted = function(folder, dataIndex)
    {
		if (dataIndex >= DATA_SHADOW) // (TODO : delete exported variations and redo template)
			return;
		
        if (dataIndex == DATA_VARIATION)
			return; // ignore variations (TODO : delete exported variations and redo template)
		
		var folderPath = folder.getPath();
		
		//angle
		if (dataIndex == DATA_ANGLE)
			folderPath = '/' + folder.parent.parent.parent.name + '/' + folder.parent.parent.name + '/' + folder.name;
		
        for (var i = 0; i < COLLECTION_SUBDIRS.length; i++)
        {
			dir_delete(this.collectionPath + COLLECTION_SUBDIRS[i] + folderPath);
        }
		
		if (dataIndex == DATA_SKIN)
		{
			//Delete settings
			var path = this.collectionPath + COLLECTION_SETTINGS + folderPath;
			dir_delete(path);
		}
    }
	
		
	///////////////////////////////////////////////////////////////////////////////
	// COPY RENDER FILES
	///////////////////////////////////////////////////////////////////////////////
	
	//USED TO OPEN MOTHER COMPO (external used only)
    this.copyRenderFiles = function(angle)
    {
		//Origin (render path subdir)
		//we use only FULL for compos
		var renderPath = this.renderPath + angle.parent.parent.getPath().substring(1) + '/' + RENDER_BODY_PARTS + '/' + RENDER_FULL + '/FF/SK';
		
		//Copy
		return this.copyToLocal(angle, renderPath);
	}
		
	this.copyToLocal = function(angle, skPath, idPath)
    {
		//Destination
        var destPath = COMPO_INPUT + '/';

		//Clean files in dir
		dir_delete(destPath, true);
		
		var ok = this.applyCopy(angle.name, skPath, destPath, 'SK');
		if (typeof(idPath) != 'undefined')
			ok &= this.applyCopy(angle.name, idPath, destPath, 'ID');
		return ok;
	}

	this.copyToLocalIDForShadow = function(angle, skPath, idPath)
    {
		//Destination
        var destPath = COMPO_INPUT + '/';
		var ok = this.applyCopy(angle.name, idPath, destPath, 'ID');
		return ok;
	}
    
    
	this.applyCopy = function(angleName, renderPath, destPath, prefix)
	{
		//Copy from render path to local input dir
		var errorCopy = false;
		
		var files = file_get_all(renderPath, '*' + angleName + FILE_EXT_PNG); // filter by angle
		Log('copying ' + files.length + ' render files for angle ' + angleName + ' from ' + renderPath + ' to ' + destPath);
		if (files.length == 0)
		{
			LogError('NO FILES for angle ' + angleName + ' IN ' + renderPath);
			errorCopy = true;
		}
		else if (!this.copyToDest(files, destPath, prefix))
			errorCopy = true;

		return !errorCopy;
    };
	
	//Copy with renaming and checking errors
	this.copyToDest = function(files, destPath, prefix)
	{
		for (var j = 0; j < files.length; j++)
		{
			var file = files[j];
			var fileName = this.renameRenderFile(file.name, prefix);
			
			if (fileName === null)
			{
				LogError('FATAL ERROR RENAMING RENDER FILE ' + file.absoluteURI + ' : ID/SK/SH/PL not found in filename.');
				return false;
			}	
			
			//Copy to destination
			var dest = destPath + fileName;
			
			if (!file.copy(dest))
			{
				LogError('FATAL ERROR COPY FILE ' + file.absoluteURI + ' to ' + dest);
				return false;
			}
		}
		return true;
	};

	//Return render filename for input dir
	this.renameRenderFile = function(fileName, prefix)
	{
		var endIndex = 8;
		if (fileName.length > 8 && fileName[fileName.length - endIndex - 1] == '.')
			endIndex = 9;
		
		//Remove right part (angle = 4 digits)
		fileName = fileName.substring(0, fileName.length - endIndex) + fileName.substring(fileName.length - 4);
		
		//Remove left part (collection_pose)
		var index = fileName.indexOf('VRay');
		if (index < 0)
		{
			if (fileName.length >= 6)
			{
				var end = fileName.substring(fileName.length - 6);
				if (end == 'SK.png' || end == 'ID.png' || end == 'PL.png')
					return fileName; // not an error for this files
				if (fileName == '.Alpha.png' || fileName == '.RGB_color.png')
					return fileName; // not error neither
			}
			
			LogError('VRay not found in ' + fileName);
			return fileName;
		}

		return prefix + '_' + fileName.substring(index);
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	// COMPOSITION (2D templates)
	///////////////////////////////////////////////////////////////////////////////



	//Main method (called after readRenderPath)
	this.processCompositions = function()
	{
		Log('checking compositions');
			
		//Prepare arrays
		this.missingCompos = {};
		this.composToLaunch = {};
		this.templatesToUpdate = {};
		
		//Read collection directory (recursive method, will fill arrays above)
		this.parseCollection(this.root, -1);
		
		//Prepare missing spec compos
		this.prepareSpecCompos();
	
		//Ask for launch
		this.launchCompos();
		
        //We update the mask 
         this.updateMask();
        
		//Update final templates
		this.updateTemplates();
		
		//Show fatal errors		
		if (this.errorMissingFile)
		{
			alert('Some file(s) where missing !');
			LogError('FATAL ERROR MISSING COMPO FILE(S)');
		}
		
		if (this.errorCompo)
		{
			alert('Something went wrong !');	
			LogError('FATAL ERROR COMPO');
		}
	}


    //Main method (called after readRenderPath)
	this.processFace = function()
	{
		Log('checking working file');
			
		//Prepare arrays
		var templatesToUpdateForFaces = [];
		
		//we get all the model in the folder to process
        var models = dir_get_all(this.workingPath);
        
        //we loop in all model folders to find the different expression
        for (var i = 0; i < models.length; i++)
        {
                Log('starting model ' + models[i].name);
                
				//we get all the expression (1 per tif)
                 var newFaces = file_get_all(models[i].fullName, FILE_EXT_TIF);

                // on recupere MORPHO-POSE-ANGLE-FACE du nom de fichier
                for (var j = 0; j < newFaces.length; j++)
                {
                    var filename = newFaces[j].name.substring(0, newFaces[j].name.length - 4);
                    var splittedName = filename.split('-');
                    var morpho = splittedName[0];
                    var pose = splittedName[1];
                    var angle = splittedName[2];
                    var FaceNumber = "Face_" + splittedName[3];

                                 
                    //on cherche le template correspondant
                    var templatePath = this.templatePath + '\\' + morpho+ '\\' + pose + '\\' + angle + '\\' ;
                    var templateFolder = new Folder (templatePath);
                    
                    if( !templateFolder.exists ) 
                    {
                        LogError('Can not find template folder ' + templatePath + ' for expression ' + newFaces[j].fullName);
                        Log('Can not find template folder ' + templatePath + ' for expression ' + newFaces[j].fullName);
                        continue;
                     }
                       
                    var templateFile =  new File(templatePath + "\\FACE.tif");
                    if( !templateFile.exists ) 
                    {
                        LogError('Can not find template ' + templateFile.fullName + ' for expression ' + newFaces[j].fullName);
                        Log('Can not find template folder ' + templatePath + ' for expression ' + newFaces[j].fullName);
                        continue;
                    }                 
                   
                   //on ouvre le template
                   var templateDoc = open(templateFile);             
                   var newExpressionDoc = open(newFaces[j]);
                   
                   //on cherche le groupe pour ce modele
                   
                   var founded = false;
                   var layerSetModel = null;
                   for (var k = 0; k < templateDoc.layerSets.length; k++)
                   {
                       layerSetModel = templateDoc.layerSets[k];
                       
                       if ( layerSetModel.name == models[i].name )
                       {
                            founded = true;
                            break;
                        }
                   }
                    
                    
                    app.activeDocument = templateDoc;
                    
               
                    if( ! founded )
                    {
                         layerSetModel = templateDoc.layerSets.add();
                         layerSetModel.name = models[i].name ;
                         layerSetModel.move(templateDoc.layers[templateDoc.layers.length - 1], ElementPlacement.PLACEAFTER);
                     }
                    else
                    {
                        //on clear le skin tone
                       // layerSetModel.layers.getByName("Skin_Tone").remove();
                     }
                    
                 
                     
                     //on parcours tous les visage deja present et on regarde si un exist deja avec ce nom (si oui on le supprime). 
                     for (var k = 0; k <layerSetModel.layerSets.length; k++)
                    {
                       if ( layerSetModel.layerSets[k].name == FaceNumber )
                       {
                            layerSetModel.layerSets[k].remove();
                            break;
                       }
                    }
                
                
                   
                
                    app.activeDocument = newExpressionDoc;
                    //on recupere le groupe du model a integrer
                    
                   
                    
                    
                    if( ! founded )
                    {
                        var layerDuplicate = newExpressionDoc.layers.getByName("Skin_Tone").duplicate(layerSetModel, ElementPlacement.INSIDE); 
                        //addedLayer.push(layerDuplicate);
                    }
                    else
                    {
                        var layerDuplicate = layerSetModel.layers.getByName("Skin_Tone");
                       // addedLayer.push(layerDuplicate);
                     }
                    
                    
                    app.activeDocument = templateDoc;
                    //on recupere le groupe du model a integrer
                    
                     var expressionGroup = layerSetModel.layerSets.add();
                    expressionGroup.name = FaceNumber;
                    
                    
                    app.activeDocument = newExpressionDoc;
                    //on recupere le groupe du model a integrer
                    
                    for( var k = newExpressionDoc.layerSets[0].artLayers.length-1; k >=0; k--)
                    {
                        var layerDuplicate = newExpressionDoc.layerSets[0].artLayers[k].duplicate(expressionGroup, ElementPlacement.INSIDE); 
                       
                    }
                
                    
                    
                    app.activeDocument = templateDoc;
                    
                    
                    //on refait les layer comp
                    // this.hideAllLayersAndGroups(app.activeDocument);
                     
                     
                      for( var k = 0; k < templateDoc.layerComps.length ; k++)
                      {
                          //if( templateDoc.layerComps[k].name != "HEAD_1" )                        
                            templateDoc.layerComps[k].remove();
                            k--;
                     }
                 
                 
                 
                    for (var k = 0; k < templateDoc.layerSets.length; k++)
                   {
                       
                       
                       layerSetModel = templateDoc.layerSets[k];
                       for (var l = 0; l <layerSetModel.layerSets.length; l++)
                        {
                            this.hideAllLayersAndGroups(templateDoc);
                             layerSetModel.visible = true;
                             
                              layerSetModel.layerSets[l].visible = true;
                            var layerSkinTone = layerSetModel.layers.getByName("Skin_Tone");
                            layerSkinTone.visible = true;
                            for (var m = 0; m <layerSetModel.layerSets[l].layers.length; m++)
                            {
                                layerSetModel.layerSets[l].layers[m].visible = true;
                            }
                            
                            var layerComp = templateDoc.layerComps.add(layerSetModel.name + "_" + layerSetModel.layerSets[l].name , 'set visibility of layers', false, false, true);
                        }
                   }
                    
                    newExpressionDoc.close(SaveOptions.DONOTSAVECHANGES);
                    templateDoc.close(SaveOptions.SAVECHANGES);
                    
                    
                    
                    
                    var templateIsToUpdate = true;
                    var mainTemplateFile = templatePath + "\\" + morpho + "_" + pose + "_" + angle + ".tif";
                    for (var k = 0; k < templatesToUpdateForFaces.length; k++)
                   {
                        if(templatesToUpdateForFaces[k] == mainTemplateFile) templateIsToUpdate = false;
                    }
                    if( templateIsToUpdate )
                        templatesToUpdateForFaces.push(mainTemplateFile);
                   
                    
                    
                     Log('Expression ' + newFaces[j].fullName + ' correctly integrated');
                }
        }
    
    
        for (var k = 0; k < templatesToUpdateForFaces.length; k++)
        {
            //MD2_POSE_A_0000
            var mainTemplateFile =  new File(templatesToUpdateForFaces[k] );
            app.activeDocument = open_psb(mainTemplateFile);
            layer_update_all_modified();
            app.activeDocument.close(SaveOptions.SAVECHANGES);
        }
        
        return true;
	}

	//Recursive check spec compo, stopping at 'bottom'
	this.parseCollection = function(folder, dataIndex)
	{
		//Bottom hierarchy
		if (dataIndex == DATA_ANGLE || dataIndex == DATA_SHADOW_ANGLE)
		{
			var angle = folder;
			var compoPath = '';
			
			//TODO : parameter
			//if (angle.name != '0137') return; //TMP
			
			if (dataIndex == DATA_SHADOW_ANGLE)
			{
				compoPath = this.collectionPath + COLLECTION_SETTINGS + '/' + angle.parent.parent.parent.name + '/SH_';
				var mothersubdirs = {'AS': 'ARM', 'BU': 'BUST', 'HF': 'FLOOR', 'FF': 'FLOOR'}; 
				var prefix = angle.parent.name.substring(0, 2);
				if (!mothersubdirs.hasOwnProperty(prefix))
				{
					LogError('mother subdir not found for shadow : ' + angle.parent.name);
					return;
				}
				compoPath += mothersubdirs[prefix] + '/' + angle.parent.parent.parent.name + '_SH_' + mothersubdirs[prefix];
			}
			else
			{
				//Spec compo
				var anglePath = '/' + angle.parent.parent.parent.name + '/' + angle.parent.parent.name + '/' + angle.name;
				var fileName = anglePath.substring(1).replace(/\//g, '_');
				compoPath = this.collectionPath + COLLECTION_SUBDIRS[COLLECTION_SUBDIR_COMPO] + anglePath + '/' + fileName;
			}
			
			compoPath += COLLECTION_MOTHER_COMPO + FILE_EXT_XML;
			
			//No spec compo
			if (!this.checkCompo(compoPath, angle))
			{
				if (!this.missingCompos.hasOwnProperty(compoPath))
					this.missingCompos[compoPath] = [];
				this.missingCompos[compoPath].push(angle);
			}
			
			//Add process to launch
			else if (angle.isNew)
			{	
				if (!this.composToLaunch.hasOwnProperty(compoPath))
					this.composToLaunch[compoPath] = [];
				this.composToLaunch[compoPath].push(angle);
			}
		}
		
		//shadows
		if (dataIndex == DATA_POSE)
		{
			for (var name in folder.shadows)
			{
				this.parseCollection(folder.shadows[name], DATA_SHADOW);
			}
		}
		
		//Go down
		for (var name in folder.children)
		{
			this.parseCollection(folder.children[name], dataIndex + 1);
		}
	}
	
	//Check if compo exists and check last modified date
	this.checkCompo = function(compoPath, angle)
	{
		var compoFile = new File(compoPath);
		if (!compoFile.exists)
			return false;
		
		//Compare date
		if (compoFile.modified > angle.date)
		{
			//angle.date = compoFile.modified; we save the new date after compo has been launched
			angle.isNew = true;
		}
		
		return true;
	}
		
	//Prepare missing spec compos
	this.prepareSpecCompos = function()
	{
		var totalCount = 0;
		for (var compoPath in this.missingCompos)
		{	
			var isShadow = compoPath.indexOf('/SH_') >= 0;
			if (isShadow)
			{
				LogError('missing mother compo shadow ' + compoPath);
				continue;
			}
				
			//Mother compo
			var angles = this.missingCompos[compoPath];
			var skin = angles[0].parent.parent.parent; // all 'angles' share the same skin
			var motherPath = this.collectionPath + COLLECTION_SETTINGS + skin.getPath() + '/' + skin.name + COLLECTION_MOTHER_COMPO + FILE_EXT_XML;

			//Check mother compo
			var motherXml = new File(motherPath);
			if (!motherXml.exists)
			{
				this.errorMissingFile = true;
				LogError('missing mother compo ' + motherPath);
				return;
			}
			
			//Copy mother compo for spec
			var motherPsb = new File(motherPath.replace(FILE_EXT_XML, FILE_EXT_PSB));
			motherXml.copy(compoPath);
			motherPsb.copy(compoPath.replace(FILE_EXT_XML, FILE_EXT_PSB));
			
			new File(LOCAL_COMPOSITOR + COMPO_BAT_OPEN).copy(compoPath.substring(0, compoPath.lastIndexOf('/')) + '/' + COMPO_BAT_OPEN);
		
			//Spec compo ready (will be launch next time)
			Log("SPEC COMPO INITED : " + compoPath);
			
			totalCount++;
		}
		
		if (totalCount > 0)
			alert(totalCount + " spec compos have been inited, check output.log for full list.");
	}	
	
	//Launch compos ready
	this.launchCompos = function()
	{
		//Compute message before launch
		var maxToShow = 30;
		var str = '';
		var compoCount = 0;
		var totalVariations = 0;
		for (var compoPath in this.composToLaunch)
		{
			var angles = this.composToLaunch[compoPath];
			totalVariations += angles.length;
			if (compoCount++ >= maxToShow) continue;
			
			str += "\n" + compoPath.substring(compoPath.lastIndexOf('/') + 1) + " for " + angles.length + " variations : ";
			for (var i = 0; i < angles.length; i++)
			{
				var variation = angles[i].parent;
				str += variation.name + ' ';
			}
		}
		
		if (compoCount == 0)
			return;
		
		//Show message to launch
		str = 'Compos to launch : ' + compoCount + str;
		if (compoCount > maxToShow)
			str += "\n... and " + (compoCount - maxToShow) + ' others compos to launch...';
		str += "\nTotal variations to generate: " + totalVariations;
		Log(str);
		if (confirm(str))
		{
			//Process all compos
			var startTime = new Date().getTime();
		
			var i = 0;
			for (var compoPath in this.composToLaunch)
			{
				i++;
				var angles = this.composToLaunch[compoPath];
				var msg = 'PROCESSING COMPO ' + i + '/' + compoCount + " for " + angles.length + " variations : " + compoPath;
				Log(msg);
				this.launchCompo(compoPath, angles);
			}
			
			var deltaTime = new Date().getTime() - startTime;
			var timeStr = this.formatDuration(deltaTime);
			Log('Total Compo Duration = ' + timeStr);
		}
	}

	
	//Main method : open compo and export all needed variations
	this.launchCompo = function(compoPath, angles)
	{
		//Get action in settings dir for this skin (all angles share this skin because they use the same compo)
		var angle = angles[0];
		var skin = angle.parent.parent.parent;
		var actionPath = this.collectionPath + COLLECTION_SETTINGS + skin.getPath() + '/' + ACTION_FILE_NAME + '.atn'; 
		
		
		//Load action
		var isShadow = compoPath.indexOf('/SH_') >= 0;
		//if (!isShadow)
		{
			this.actionLoaded = action_load(actionPath);
			if (!this.actionLoaded)
			{
				LogError('Error loading action -> ' + actionPath + ' not found');
				this.errorMissingFile = true;
				return;
			}
		}
		/*else		
			this.actionLoaded = false;
		*/
		var startTime = new Date().getTime();
		
		var psbPath = compoPath.replace(FILE_EXT_XML, FILE_EXT_PSB);
		
		//Clean files in input
		
		dir_delete(COMPO_INPUT, true);
		
		//Copy spec compo in input dir
		var compo = new File(psbPath);
		if (!compo.exists)
		{
			LogError('compo ' + psbPath + ' not found');
			this.errorMissingFile = true;
			return;
		}
		compo.copy(COMPO_INPUT + '/' + compo.name);
		
		
		//Open spec compo in input dir
		var doc = open_psb(new File(COMPO_INPUT + '/' + compo.name));
		if (doc === null)
		{
			LogError('FATAL ERROR OPENING COMPO FILE ' +  COMPO_INPUT + '/' + compo.name); 
			this.errorCompo = true;
			return;
		}
		
		//For each variations (angles.parent)
		
		for (var i = 0; i < angles.length; i++)
		{
			//Export
			Log('Exporting variation ' + (i+1) + '/' + angles.length + ' : ' + angles[i].parent.name);
			
			this.applyCompo(compoPath, angles[i]);
		}

		//All done, close compo		
		close_document();

		var deltaTime = new Date().getTime() - startTime;
		var timeStr = this.formatDuration(deltaTime);
		var average = this.formatDuration(Math.round(deltaTime / angles.length));
		Log('COMPO DONE: ' + compoPath);
		Log(angles.length + ' variations generated in ' + timeStr + '(' + average + '/variation)');
	};
	
	
	this.getRenderPath = function(angle, isShadow)
	{	
		//Get render files path
		var variation = angle.parent;
		var prefix = variation.name.substring(0, 2);
		var subdirs = {'AS': 'ARMS', 'BU': 'BUSTS', 'HE': 'HEADS', 'LE': 'LEGS', 'CL': 'CLOTHES', 'HF': 'FLOOR', 'FF': 'FLOOR'}; // TODO : ugly
		if (!subdirs.hasOwnProperty(prefix))
		{
			LogError('Error finding render file subdir for :' + variation.getPath());
			this.errorCompo = true;
			return;
		}
		
		var renderPath = this.renderPath + angle.parent.parent.getPath().substring(1) + '/' + (isShadow ? RENDER_SHADOWS : RENDER_BODY_PARTS) + '/';
		renderPath += subdirs[prefix] + '/'	
		
		if (isShadow && subdirs[prefix] != 'FLOOR')
		{
			var splitted = variation.name.split('_FROM_');
			if (subdirs[prefix] == 'ARMS')
				renderPath += 'FROM_' + splitted[1] + '/' + splitted[0] + '/';
			else
				renderPath += splitted[0] + '/FROM_' + splitted[1] + '/';
		}
		else
			renderPath += variation.name + '/';
		
		return renderPath;
	}

    this.getRenderPathForShaw = function(angle, type)
	{	
		//Get render files path
		var variation = angle.parent;
		var prefix = variation.name.substring(0, 2);
		var subdirs = {'AS': 'ARMS', 'BU': 'BUSTS', 'HE': 'HEADS', 'LE': 'LEGS', 'CL': 'CLOTHES', 'HF': 'FLOOR', 'FF': 'FLOOR'}; // TODO : ugly
		if (!subdirs.hasOwnProperty(prefix))
		{
			LogError('Error finding render file subdir for :' + variation.getPath());
			this.errorCompo = true;
			return;
		}
		
		var renderPath = this.renderPath + angle.parent.parent.getPath().substring(1) + '/' + RENDER_BODY_PARTS + '/';
		renderPath += subdirs[prefix] + '/'	
		
         var renomage = variation.name;
         
         for (var i = 0; i < type; i++)
            renomage = renomage.substring(0, renomage.lastIndexOf("_"));
         
         
        
		renderPath +=  renomage + '/';
		
		return renderPath;
	}
	
	//Generate compo (copy needed files and export variation)
	this.applyCompo = function(compoPath, angle)
	{	
		
		var isShadow = compoPath.indexOf('/SH_') >= 0;
	
		//Get render files path
		var variation = angle.parent;
		var prefix = variation.name.substring(0, 2);
		
		var renderPath = this.getRenderPath(angle, isShadow);
        
         
		
		
		//Cutted template part
		var cutsubdirs = {'AS': 'ARM', 'BU': 'BUST', 'HE': 'FACE', 'LE': 'LEG', 'CL': 'CLOTH', 'HF': 'FLOOR', 'FF': 'FLOOR'}; // TODO : ugly
		if (!cutsubdirs.hasOwnProperty(prefix))
		{
			LogError('Error finding cut subdir for :' + variation.getPath());
			this.errorCompo = true;
			return;
		}

		var cutSubdir = cutsubdirs[prefix];
		
		var allOk = true;
		if (isShadow)
		{
			if (cutSubdir == 'FLOOR')
			{
				allOk = 	this.copyToLocal(angle, renderPath)
						&& 	this.exportVariation(compoPath, angle, cutSubdir + '_SH');
			}
			else
			{
				if (cutSubdir == 'ARM')
				{
                        var renderPathIDForShadow = this.getRenderPathForShaw(angle, 2);
                        
					//Render left
					allOk = 	this.copyToLocal(angle, renderPath + 'L') /*on recupere l ID correspondant */
                                && this.copyToLocalIDForShadow(angle, renderPath + 'L', renderPathIDForShadow + 'L/ID/ID_Solo')
							&&	this.exportVariation(compoPath, angle, cutSubdir + '_L')
					//Render right
							&& this.copyToLocal(angle, renderPath + 'R')
                                && this.copyToLocalIDForShadow(angle, renderPath + 'R', renderPathIDForShadow + 'R/ID/ID_Solo')
							&& this.exportVariation(compoPath, angle, cutSubdir + '_R');
				}
				else
				{
                        var renderPathIDForShadow = this.getRenderPathForShaw(angle, 3);
					//Render left
					allOk = 	this.copyToLocal(angle, renderPath + 'L')
                                && this.copyToLocalIDForShadow(angle, renderPath , renderPathIDForShadow + 'ID/ID_Solo')
							&&	this.exportVariation(compoPath, angle, cutSubdir + '_SH_FROM_ARM_L')
					//Render right
							&& this.copyToLocal(angle, renderPath + 'R')
                               && this.copyToLocalIDForShadow(angle, renderPath , renderPathIDForShadow + 'ID/ID_Solo')
							&& this.exportVariation(compoPath, angle, cutSubdir + '_SH_FROM_ARM_R');
				}
			}
		}
		else
		{
			
			if (prefix == 'AS' || prefix == 'LE') // ARM or LEG
			{
				//Render left
				allOk = 	this.copyToLocal(angle, renderPath + 'L/SK', renderPath + 'L/ID/ID_Solo')
						&&	this.exportVariation(compoPath, angle, cutSubdir + '_L')
				//Render right
						&& this.copyToLocal(angle, renderPath + 'R/SK', renderPath + 'R/ID/ID_Solo')
						&& this.exportVariation(compoPath, angle, cutSubdir + '_R');
			}
			else
			{
				allOk = 	this.copyToLocal(angle, renderPath + 'SK', renderPath + 'ID/ID_Solo')
						&& 	this.exportVariation(compoPath, angle, cutSubdir);
			}
		}
		
		if (!allOk)
			this.errorCompo = true;
	}
		
	//Cut and export variation
	this.exportVariation = function(compoPath, angle, cutSubdir)
	{
		var doc = app.activeDocument;

		
		//Go back to first history state (open doc)
		doc.activeHistoryState = doc.historyStates[0];
		
		Log('Replacing png in compo');
		
		//Open compo xml (will load settings and load the files too)
		try 
		{ 
			if (!open_mother_compo(new File(compoPath), COMPO_INPUT))
			{

				LogError('missing file for compo ' + compoPath);
				this.errorCompo = true;
				return false;
			}
		}
		catch (e)
		{
			alert('FATAL ERROR OPENING COMPO XML ' +  compoPath); 
			LogError('FATAL ERROR OPENING COMPO XML ' +  compoPath); 
			LogError(e);
			this.errorCompo = true;
			return false;
		}
		this.motherOpened = true;
		
		//Save initial visibility
		var layerComp = doc.layerComps.add('initial config', 'set visibility of layers', false, false, true);
	
    
        var isShadow = compoPath.indexOf('/SH_') >= 0;
		
		//Launch compo action
		if (this.actionLoaded && !isShadow)
		{
			var index = cutSubdir.indexOf('_'); if (index < 0) index = cutSubdir.length;
			var actionName = cutSubdir.substring(0, index);
			Log('Exec action ' + ACTION_COMPO + actionName);
			try { action_exec(ACTION_COMPO + actionName, ACTION_FILE_NAME); }   
			catch(e) {
				LogError('FATAL ERROR EXEC ACTION -> ' + ACTION_COMPO + actionName + ' in ' + ACTION_FILE_NAME);
				this.errorCompo = true;
				return;
			}
		}
			
		//Apply clipping
		//if (!)
        var isIDpresent = true;
		
        Log('we try to do the clipping (looking for ID)');
        var clippingPath = COMPO_INPUT + '/ID_VRayDiffuseFilter_ID' + FILE_EXT_PNG; // TODO : define
        if (!new File(clippingPath).exists)
        {
            clippingPath = COMPO_INPUT + '/ID_VRayDiffuseFilter' + FILE_EXT_PNG; // TODO : define
            if (!new File(clippingPath).exists)
            {
                LogError(clippingPath + ' not found for ' + angle.getPath());
                  isIDpresent = false;
                  
                  if( !isShadow )
                  {
                    this.errorCompo = true;
                    return false;
                  }
            }
        }
            
        if( isIDpresent )
        {
            open_as_layer(clippingPath);
            action_exec("CANVAS SIZE", ACTION_FILE_NAME); 
            //Hide all except ID and black background
            this.hideAllLayers(doc);
            doc.artLayers[0].visible = true;
            this.showBlackBg(doc);
            
            //Make selection with rgb channel
            var channel = doc.channels[0];//.getByName("RVB"); only 3 channel, R, V, and B, not the merged one.. seems to work with R only
            doc.selection.load(channel, SelectionType.REPLACE);
                        
            //Remove clipping file
            doc.artLayers[0].remove(); 
            
            //Show layers back
            layerComp.apply();
        }
		else
		{
			//select all for shadow
            
            action_exec("CANVAS SIZE", ACTION_FILE_NAME); 
			doc.selection.selectAll();
             
		}
		
		//Copy visible layers in selection
		doc.selection.copy(true);
		
		
		
		//Add variation in template part
		var templatePath = angle.parent.parent.parent.name + '/' + angle.parent.parent.name + '/' + angle.name;
		this.updateTemplatePart(templatePath, cutSubdir, angle);
	
		//Store new date for this angle
		angle.date = new Date();
			
		//Store last update date for root (for info)
		this.root.date = angle.date;
		
		//Add template to update
		if (!this.templatesToUpdate.hasOwnProperty(templatePath))
			this.templatesToUpdate[templatePath] = [];
		this.templatesToUpdate[templatePath].push(angle);
		
		return true;
	};
	
	this.hideAllLayers = function(doc)
	{
		for (var i = 0; i < doc.layers.length; i++)
		{
                doc.layers[i].visible = false;
		}
	};

    this.hideAllLayersAndGroups = function(doc)
	{
		for (var i = 0; i < doc.layers.length; i++)
		{
            var layer = doc.layers[i];
            if( layer.typename == "LayerSet" )
            {
                layer.visible = false;
                this.hideAllLayersAndGroups(layer);
            }
            else
                layer.visible = false;
		}
	};
	
	this.showBlackBg = function(doc)
	{
		try 
		{
			var layer = doc.layers.getByName('BACKGROUND');
			layer.visible = true;
			layer = layer.layers.getByName('BLACK');
			layer.visible = true;
		}
		catch(e)
		{
			LogError('black layer not found');
		}
	};
	
	
	//Paste selection of current variation into template part
	this.updateTemplatePart = function(templatePath, part, angle)
	{
		//Init template if not exist
		var templateName = templatePath.replace(/\//g, '_');
		templatePath = this.collectionPath + COLLECTION_SUBDIRS[COLLECTION_SUBDIR_TEMPLATE] + '/' + templatePath + '/';
		
		if (!this.initTemplate(templatePath, templateName))
			return;
		
		//Open tif
		var partFile = new File(templatePath + part + FILE_EXT_TIF);
		if (!partFile.exists)
		{
			LogError('template part ' + partFile.fsName + ' not found');
			this.errorMissingFile = true;
			return;
		}
				
		
		//Paste into template part file (creating layer)
		//TODO : check with dialog if error
		var prev = app.displayDialogs;
		app.displayDialogs = DialogModes.NO;
		var newDoc = open_psb(partFile);
         var layerPresent = [];
         for (var i = 0; i < newDoc.artLayers.length; i++)
        {
			layerPresent.push(newDoc.artLayers[i].name);
         }
     
		pasteInPlace();//newDoc.paste();
        
        // defringe
        
        action_exec("DEFRINDGE", ACTION_FILE_NAME); 
        
        
        var newLayer = null;
         for (var i = 0; i < newDoc.artLayers.length; i++)
        {
            var notHereBefore = true;
            for (var j = 0; j < layerPresent.length; j++)
            {
                if( layerPresent[j] == newDoc.artLayers[i].name )
                {
                    notHereBefore = false;
                    
                }
            }
            if( notHereBefore )
            {
                newLayer = newDoc.artLayers[i];
                notHereBefore = false;
             }
        }
        
        
		//var newLayer = newDoc.artLayers[0];
		app.displayDialogs = prev;
		
		//Check existing
		var variation = angle.parent.name;
		
		if (variation.substring(0, 4) == 'LEGS') //UGLY SPECIFIC
			variation = variation.substring(5);
            
            
        if (variation.substring(0, 4) == 'BUST') //UGLY SPECIFIC
        {
            var split = variation.split('_');
            if(split.length > 2 )
            {
                variation = split[2] + '_' + split[3] + '_' + split[1] + '_' +  split[0];
             }
         }
			
		var isNew = false;
		var oldLayer = null;
		try { oldLayer = newDoc.layers.getByName(variation); }
		catch (e) { isNew = true; }
		
		//Remove old
		if (!isNew)
		{
			oldLayer.remove();
			try { newDoc.layerComps.getByName(variation).remove(); }
			catch (e) { LogError('old layer comp ' + variation + ' not found in ' + partFile.fsName); }
		}

		newLayer.name = variation;
		//layer_add_mask_layer(newLayer); // add mask
			
		//Add layer comp
		this.hideAllLayers(newDoc);
		newLayer.visible = true;
		
		newDoc.layerComps.add(variation, 'select ' + variation, false, false, true);
	
		//Save and close new doc
		newDoc.close(SaveOptions.SAVECHANGES);
		Log(variation + (isNew ? ' added to ' : ' updated in ') + partFile.fsName);
	}


	this.initTemplate = function(destPath, destName)
	{
		var settingsPath = this.collectionPath + COLLECTION_SETTINGS + '/' + COLLECTION_TEMPLATE + '/';
		destName = destName + FILE_EXT_TIF;
		
		var file = new File(settingsPath + 'TEMPLATE.tif');
		var dest = new File(destPath + destName);
		var ok = true;
		
		
		
		if (!dest.exists && !file.copy(dest))
		{
			ok = false;
			LogError('error copy ' + settingsPath + 'TEMPLATE.tif to ' + dest);
		}
		for (var i = 0; i < TEMPLATE_PARTS.length; i++)
		{
			var fileName = TEMPLATE_PARTS[i] + FILE_EXT_TIF;
			var file = new File(settingsPath + fileName);
			var dest = new File(destPath + fileName);
			if (!dest.exists && !file.copy(dest))
			{
				ok = false;
				LogError('error copy ' + settingsPath + fileName + ' to ' + destPath + fileName);
			}
		}
		
		if (!ok)
		{
			this.errorMissingFile = true;
			LogError('Error initing template -> missing files in settings or unable to copy them');
		}

		return ok;
	}
	
    this.updateMask = function()
    {
        var msgStr = '';
		var updateCount = 0;
		for (var templatePath in this.templatesToUpdate)
		{
			//Init template
            
             var splited = templatePath.split('/');
			var templateName = templatePath.replace(/\//g, '_'); 
            
           
            
             var angle = splited[2];  
    
             var pose = splited[1]; 
                      
             var morpho = splited[0];   

            
             //this.copyToLocal(angle, renderPath + 'L/SK', renderPath + 'L/ID/ID_Solo')
             
			var templateFile = new File(this.collectionPath + COLLECTION_SUBDIRS[COLLECTION_SUBDIR_TEMPLATE] + '/' + templatePath + '/' + ARM_LEFT_MASK + FILE_EXT_TIF);
			if (!templateFile.exists)
			{
				LogError('Template ' + templateFile.fsName + ' not found');
				this.errorMissingFile = true;
				continue;
			}
			
			//Update all content
			app.activeDocument = open_psb(templateFile);
			
              // on liste tous les mask qu on a dans notre dossier de rendu pour cette pose, cet angle
              
              //MD2\POSE_A\BODY_PARTS\ARMS
              
              var dirs = dir_get_all(new Folder(this.renderPath + "/"  + morpho + "/"  + pose + "/"  + RENDER_BODY_PARTS  +  "/ARMS/" ));

		
              for (var i = 0; i < dirs.length; i++)
              {
                  var variationName = dirs[i].name;
                  var dirsMask = dir_get_all(new Folder(dirs[i].fullName + "/L/ID/") );
                  for (var j = 0; j < dirsMask.length; j++)
                  {
                        var bustName = dirsMask[j].name;
                        bustName = bustName.substring( bustName.lastIndexOf("_") +1, bustName.length);
                        
                        if( bustName != "Solo" )
                        {
                            var maskPng = new File (dirsMask[j].fullName + "/.ID_VRayDiffuseFilter." + angle + FILE_EXT_PNG);
                            if( maskPng.exists)
                            {
                                
                                open_as_layer_and_apply_action(maskPng, "CANVAS SIZE", ACTION_FILE_NAME);
                                var layerAdded = app.activeDocument.artLayers[0];
                                var maskName = bustName + "_" + variationName;
                                
                                 for (var k = 1; k < app.activeDocument.artLayers.length; k++)
                                 {
                                     if( maskName == app.activeDocument.artLayers[k].name )
                                     {
                                         app.activeDocument.artLayers[k].remove();
                                         k--;
                                      }
                                 }
                                 layerAdded.name = maskName;
                                 
                                
                            }
                        }
                    }
              }
          
          
                for( var k = 0; k < app.activeDocument.layerComps.length ; k++)
                {
                        app.activeDocument.layerComps[k].remove();
                        k--;
                 }

                for (var k = 0; k < app.activeDocument.layers.length; k++)
               {
                    //Add layer comp
                    this.hideAllLayers(app.activeDocument);
                    app.activeDocument.layers[k].visible = true;
                    app.activeDocument.layerComps.add(app.activeDocument.layers[k].name, 'select ' + app.activeDocument.layers[k].name, false, false, true);
                }
            
               app.activeDocument.close(SaveOptions.SAVECHANGES);
              
              //bras droit
              
              templateFile = new File(this.collectionPath + COLLECTION_SUBDIRS[COLLECTION_SUBDIR_TEMPLATE] + '/' + templatePath + '/' + ARM_RIGHT_MASK + FILE_EXT_TIF);
			if (!templateFile.exists)
			{
				LogError('Template ' + templateFile.fsName + ' not found');
				this.errorMissingFile = true;
				continue;
			}
			
			//Update all content
			app.activeDocument = open_psb(templateFile);
			
             
              
              //MD2\POSE_A\BODY_PARTS\ARMS

              var dirs = dir_get_all(new Folder(this.renderPath + "/"  + morpho + "/"  + pose + "/"  + RENDER_BODY_PARTS  +  "/ARMS/" ));

		
              for (var i = 0; i < dirs.length; i++)
              {
                  var variationName = dirs[i].name;
                  var dirsMask = dir_get_all(new Folder(dirs[i].fullName + "/R/ID/") );
                  for (var j = 0; j < dirsMask.length; j++)
                  {
                        var bustName = dirsMask[j].name;
                        bustName = bustName.substring( bustName.lastIndexOf("_") +1, bustName.length);
                        
                        if( bustName != "Solo" )
                        {
                            var maskPng = new File (dirsMask[j].fullName + "/.ID_VRayDiffuseFilter." + angle + FILE_EXT_PNG);
                            if( maskPng.exists)
                            {
                                
                                open_as_layer_and_apply_action(maskPng, "CANVAS SIZE", ACTION_FILE_NAME);
                                action_exec("DEFRINDGE", ACTION_FILE_NAME); 
                                var layerAdded = app.activeDocument.artLayers[0];
                                var maskName = bustName + "_" + variationName;
                                
                                 for (var k = 1; k < app.activeDocument.artLayers.length; k++)
                                 {
                                     if( maskName == app.activeDocument.artLayers[k].name )
                                     {
                                         app.activeDocument.artLayers[k].remove();

                                         k--;
                                      }
                                 }
                                layerAdded.name = maskName;
                            }
                                 
                        }
                    }
              }
            
              
              for( var k = 0; k < app.activeDocument.layerComps.length ; k++)
              {
                    app.activeDocument.layerComps[k].remove();
                    k--;
             }
             
             
             
                for (var k = 0; k < app.activeDocument.layers.length; k++)
               {
                    //Add layer comp
                    this.hideAllLayers(app.activeDocument);
                    app.activeDocument.layers[k].visible = true;
                    app.activeDocument.layerComps.add(app.activeDocument.layers[k].name, 'select ' + app.activeDocument.layers[k].name, false, false, true);
                 }
              
              
              
			app.activeDocument.close(SaveOptions.SAVECHANGES);
			
			var variations = [];
			for (var i = 0; i < this.templatesToUpdate[templatePath].length; i++)
				variations.push(this.templatesToUpdate[templatePath][i].parent.name);
			
			var msg = templateName + FILE_EXT_TIF + ' updated with ' + variations.join(' ');
			Log('Template ' + msg);
			msgStr += '\n' + msg;
			updateCount++;
		}

    }
	
	this.updateTemplates = function()
	{
		var msgStr = '';
		var updateCount = 0;
		for (var templatePath in this.templatesToUpdate)
		{
			//Init template
			var templateName = templatePath.replace(/\//g, '_');
			var templateFile = new File(this.collectionPath + COLLECTION_SUBDIRS[COLLECTION_SUBDIR_TEMPLATE] + '/' + templatePath + '/' + templateName + FILE_EXT_TIF);
			if (!templateFile.exists)
			{
				LogError('Template ' + templateFile.fsName + ' not found');
				this.errorMissingFile = true;
				continue;
			}
			
			//Update all content
			app.activeDocument = open_psb(templateFile);
			layer_update_all_modified();
			app.activeDocument.close(SaveOptions.SAVECHANGES);
			
			var variations = [];
			for (var i = 0; i < this.templatesToUpdate[templatePath].length; i++)
				variations.push(this.templatesToUpdate[templatePath][i].parent.name);
			
			var msg = templateName + FILE_EXT_TIF + ' updated with ' + variations.join(' ');
			Log('Template ' + msg);
			msgStr += '\n' + msg;
			updateCount++;
		}
		
		if (updateCount > 0)
			alert(updateCount + ' templates updated : ' + msgStr);
	}
}