﻿///////////////////////////////////////////////////////////////////////////////
// Defines
///////////////////////////////////////////////////////////////////////////////
var FILE_EXT_BMP = ".bmp";
var FILE_EXT_JPG = ".jpg";
var FILE_EXT_PNG = ".png";
var FILE_EXT_PSB = ".psb";
var FILE_EXT_PSD = ".psd";
var FILE_EXT_TIF = ".tif";
var FILE_EXT_XML = ".xml";
var FILE_EXT_ATN = ".atn";

///////////////////////////////////////////////////////////////////////////////
// dir_get_all - get all folders within the specified source
///////////////////////////////////////////////////////////////////////////////
function dir_get_all(sourceFolder, pattern) 
{
	var dirs = new Array();
	
	if (! (sourceFolder instanceof Folder))
		sourceFolder = new Folder(sourceFolder);
	if (typeof(pattern) == 'undefined')
		pattern = '*';
	
	var elems = sourceFolder.getFiles(pattern);
	var len = elems.length;
	for (var i = 0; i < len; i++) 
	{
		var elem = elems[i];
		if (elem instanceof Folder && elem.name[0] != '_') // ignore directories starting with _
			dirs.push(elem);
	}
	
	//Sort them by name
	dirs.sort(function(a,b) { return a.name.localeCompare(b.name); });
	
	return dirs;
}

///////////////////////////////////////////////////////////////////////////////
// file_get_all - get all files within the specified source
///////////////////////////////////////////////////////////////////////////////
function file_get_all(sourceFolder, pattern) 
{
	var files = new Array();
	
	if (! (sourceFolder instanceof Folder))
		sourceFolder = new Folder(sourceFolder);
	if (typeof(pattern) == 'undefined')
		pattern = '*';
	
	// get all files in source folder
	var elems = sourceFolder.getFiles(pattern);
	var len = elems.length;
	for (var i = 0; i < len; i++) 
	{
		var elem = elems[i];
		if (elem instanceof File)
			files.push(elem);
	}
	
	//Sort them by name
	files.sort(function(a,b) { return a.name.localeCompare(b.name); });
	
	return files;
}

///////////////////////////////////////////////////////////////////////////////
// Create directory if not exist
///////////////////////////////////////////////////////////////////////////////
function dir_create(path)
{
	var dir = new Folder(path);
	if (dir.exists)
		return true;

	return dir.create();
}

///////////////////////////////////////////////////////////////////////////////
// dir_delete - delete folder and sub-folders with all files in it, recursively
///////////////////////////////////////////////////////////////////////////////
function dir_delete(folder, keepMe)
{
    if (! (folder instanceof Folder))
        folder = new Folder(folder);

    var elems = folder.getFiles();
    var len = elems.length;
    for (var i = 0; i < len; i++)
    {
        var elem = elems[i];
        if (elem instanceof File)
            elem.remove();
        else if (elem instanceof Folder)
            dir_delete(elem);
    }

	if (keepMe !== true)
		folder.remove();
}

///////////////////////////////////////////////////////////////////////////////
// path_format - format path with / (instead of \), and with a end / for directory
///////////////////////////////////////////////////////////////////////////////
function path_format(path, isDir)
{
	path = "" + path;
	path = path.replace(/\\/g, "/");
	//Add end / for directory
	if (isDir === true && (path.length == 0 || path[path.length-1] != '/'))
		path = path + '/';
	return path;
}


///////////////////////////////////////////////////////////////////////////////
// launch_batch - Launch a batch command and wait until it's done (kind of a hack... but very useful)
///////////////////////////////////////////////////////////////////////////////
function launch_batch(file, cmd)
{
	if (! (file instanceof File))
		file = new File(file);
	
	//Write command(s)
	file.open('w');
	file.writeln(cmd);
	file.write('mkdir ___done___');
	file.close();
	
	//Make sure the ___done___ directory is not present
	var folder = new Folder('___done___');
	if (folder.exists)
		folder.remove();
	
	//Launch until ___done___ directory is created
	file.execute();
	while(!folder.exists)
	{
		$.sleep(1000);
	}
	folder.remove();
};