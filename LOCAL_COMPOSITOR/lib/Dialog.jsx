function Alert(title, msg)
{
	if (typeof(msg) == 'undefined')
	{
		msg = title;
		title = 'Alert';
	}
	
	//TODO : make a nice dialog box (Allure Systems logo, copyright, etc...)
	var box = new Window('dialog', title);
	box.panel = box.add('statictext', undefined, msg);
	box.closeBtn = box.add('button',undefined, "Close", {name:'Close'}); 
	box.closeBtn.onClick = function(){  
	  box.close();  
	};
	box.show();
}