﻿///////////////////////////////////////////////////////////////////////////////
// BASICS 
///////////////////////////////////////////////////////////////////////////////
function version_check() 
{
	if (parseInt(version, 10) < 9) 
	{
		Alert('This script requires Adobe Photoshop CS2 or higher.', 'Wrong Version', false);
		Error.runtimeError(9999, "Exit Script");
	}
}

function create_document(w, h, name)
{
	//Default params
	if (typeof(w) == 'undefined') w = 5360;
	if (typeof(h) == 'undefined') h = 8000;
	if (typeof(name) == 'undefined') name = 'mother_compo_auto';
	var doc = documents.add(w, h, 72, name, NewDocumentMode.RGB);
	doc.artLayers[0].isBackgroundLayer = false; // need to do that to allow 'add' to doc.artLayers... dumb but real
	app.activeDocument = doc;
	return doc;
}

function close_document()
{
	app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);
}

///////////////////////////////////////////////////////////////////////////////
// ACTIONS 
///////////////////////////////////////////////////////////////////////////////
function action_load(actionPath)
{
	// we clean the action list first to be sure the last version is loaded
	var actionList = action_getASets();
	for(var d in actionList)
		action_unload(actionList[d]);

	var action = new File(actionPath);
	if (action.exists) 
	{	
		app.load(action); 
		return true;
	}
	
	return false;
}

function action_unload(aSet)
{
	var desc = new ActionDescriptor(); 
	var ref = new ActionReference(); 
	ref.putName( charIDToTypeID( "ASet" ), decodeURI(aSet)); 
	desc.putReference( charIDToTypeID( "null" ), ref ); 
	executeAction( charIDToTypeID( "Dlt " ), desc, DialogModes.NO );
}

function action_getASets() 
{ 
	cTID = function(s) { return app.charIDToTypeID(s); }; 
	var i = 1; 
	var sets = [];  
	while (true) 
	{ 
		var ref = new ActionReference(); 
		ref.putIndex(cTID("ASet"), i); 
		var desc; 
		var lvl = $.level; 
		$.level = 0; 
		try
		{ 
			desc = executeActionGet(ref); 
		} 
		catch (e)
		{ 
			break;
		}
		finally 
		{ 
			$.level = lvl; 
		} 
		if (desc.hasKey(cTID("Nm  "))) 
		{ 
			var set = {}; 
			set.index = i; 
			set.name = desc.getString(cTID("Nm  ")); 
			set.toString = function() { return this.name; }; 
			set.count = desc.getInteger(cTID("NmbC")); 
			set.actions = []; 
			for (var j = 1; j <= set.count; j++)
			{ 
				var ref = new ActionReference(); 
				ref.putIndex(cTID('Actn'), j); 
				ref.putIndex(cTID('ASet'), set.index); 
				var adesc = executeActionGet(ref); 
				var actName = adesc.getString(cTID('Nm  ')); 
				set.actions.push(actName); 
			} 
			sets.push(set); 
		} 
	i++; 
	} 
	return sets; 
}

function action_exec(name, fileName)
{
	try {
		app.doAction(name, fileName);  
		return true;
	}catch(e)
	{
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
// FILES OPEN/SAVE
///////////////////////////////////////////////////////////////////////////////
function open_psb( file ) // TODO : rename : open_doc() => work for any file photoshop likes
{
	var prev = app.displayDialogs;
	app.displayDialogs = DialogModes.NO;
		
	if (! (file instanceof File))
		file = new File(file);
	try {
		var fileRef = open(file);
		app.displayDialogs = prev;
		return fileRef;
	}catch(e)
	{
		app.displayDialogs = prev;
		LogError(e);
		return null;
	}
}

function open_as_layer_and_apply_action( file, actionName, actionFile)
{
	if (! (file instanceof File))
		file = new File(file);
	
	var prev = app.displayDialogs;
	app.displayDialogs = DialogModes.NO;
	var doc = app.activeDocument;
	var ok = true;
	try {
		app.activeDocument = open(file);
		convert_bitdepth(8); // need 8bits to duplicate to 8bit doc
         action_exec(actionName, actionFile); 
        
		app.activeDocument.artLayers[0].duplicate(doc);
		close_document();
	}
	catch(e)
	{
		ok = false;
	}
	app.activeDocument = doc;
	app.displayDialogs = prev;
	return ok;
}


function open_as_layer( file )
{
	if (! (file instanceof File))
		file = new File(file);
	
	var prev = app.displayDialogs;
	app.displayDialogs = DialogModes.NO;
	var doc = app.activeDocument;
	var ok = true;
	try {
		app.activeDocument = open(file);
		convert_bitdepth(8); // need 8bits to duplicate to 8bit doc
		app.activeDocument.artLayers[0].duplicate(doc);
		close_document();
	}
	catch(e)
	{
		ok = false;
	}
	app.activeDocument = doc;
	app.displayDialogs = prev;
	return ok;
}

function save_psb( filePath ) 
{  
	function cTID(s) { return app.charIDToTypeID(s); };  
	//function sTID(s) { return app.stringIDToTypeID(s); };  

	var desc7 = new ActionDescriptor();  
	var desc8 = new ActionDescriptor();  
	desc7.putObject( cTID('As  '), cTID('Pht8'), desc8 );  
	desc7.putPath( cTID('In  '), new File( filePath ) );  
	desc7.putBoolean( cTID('LwCs'), true );  
	try {
		executeAction( cTID('save'), desc7, DialogModes.NO );  
		return true;
	}catch(e)
	{
		return false;
	}
}

function save_png(savePath)
{
	var saveFilePNG = new File(savePath);
	var saveOptionsPng = new PNGSaveOptions( );
	saveOptionsPng.compression=0;
	saveOptionsPng.interlaced=false;
	try {
		app.activeDocument.saveAs( saveFilePNG, saveOptionsPng, true ); 
		return true;
	}catch(e)
	{
		return false;
	}
}

function save_bmp(savePath)
{
	var saveFileBMP = new File(savePath);
	var saveOptionsBMP = new BMPSaveOptions();
	//saveOptionsBMP.alphaChannels = false;
	//saveOptionsBMP.depth = BMPDepthType.EIGHT;
	//saveOptionsBMP.flipRowOrder = false;
	//saveOptionsBMP.rleCompression = false;
	//saveOptionsBMP.osType = OperatingSystem.WINDOWS;
	try {
		app.activeDocument.saveAs( saveFileBMP, saveOptionsBMP, true ); 
		return true;
	}catch(e)
	{
		alert(e);
		return false;
	}
}

function save_jpg(savePath, qty ) 
{  
	if (typeof(qty) == 'undefined')
		qty = 9; //0 -> 12 (default 3)
	
	var saveFileJPG = new File(savePath);
	var saveOptions = new JPEGSaveOptions( );  
	saveOptions.embedColorProfile = true;  
	saveOptions.formatOptions = FormatOptions.STANDARDBASELINE;  
	saveOptions.matte = MatteType.NONE;  
	saveOptions.quality = qty;   

	try {
		app.activeDocument.saveAs(saveFileJPG, saveOptions, true);  
		return true;
	}catch(e)
	{
		alert(e);
		return false;
	}
} 

//Save tif with or without layers (default without)
function save_tif(savePath, withLayer) 
{  
	var saveOptions = new TiffSaveOptions();
	saveOptions.layers = withLayer === true;
	saveOptions.transparency = true;
	saveOptions.embedColorProfile = true;
	saveOptions.imageCompression = TIFFEncoding.TIFFLZW;
	saveOptions.layerCompression = LayerCompression.ZIP;
	
	var saveFile = new File(savePath);
	try {
		app.activeDocument.saveAs(saveFile, saveOptions);  
		return true;
	}catch(e)
	{
		alert(e);
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
// CONVERSIONS
///////////////////////////////////////////////////////////////////////////////
function convert_bitdepth(bitdepth) 
{
	var id1 = charIDToTypeID("CnvM");
	var convert = new ActionDescriptor();
	var id2 = charIDToTypeID("Dpth");
	convert.putInteger(id2, bitdepth);
	executeAction(id1, convert, DialogModes.NO);
}
		
function convert_image(input, tifPath, pngPath, bmpPath, w, h)
{
	if (!(input instanceof File))
		input = new File(input);
	if (!input.exists)
	{
		LogError('Error image not found ' + input);
		return false;
	}
	
	var prev = app.displayDialogs;
	app.displayDialogs = DialogModes.NO;
		
	try {
		var document = open(input);
		app.activeDocument = document;
		var needResize = w && h && (document.width.as('px') != w || document.height.as('px') != h);
		if (needResize)
			document.resizeImage(UnitValue(w, "px"), UnitValue(h, "px"), 300, ResampleMethod.BICUBIC);
		
		//Save tif
		var ok = true;
		if (tifPath != '' && (input != tifPath || needResize))
		{
			Log('Saving tif ' + tifPath);
			ok &= save_tif(tifPath);
		}
		
		//Save png
		if (pngPath != '' && (input != pngPath || needResize))
		{
			Log('Saving png ' + pngPath);
			ok &= save_png(pngPath);
		}
		
		//Save bmp
		if (ok && bmpPath != '' && (input != bmpPath || needResize))
		{
			Log('Saving bmp ' + bmpPath);
			ok &= save_bmp(bmpPath);
		}
		
		if (!ok)
			LogError('Error converting image ' + input);
		
		close_document();
		app.displayDialogs = prev;	
		
		return ok;
	}catch(e)
	{
		alert(e);
		app.displayDialogs = prev;
		return false;
	}
}


///////////////////////////////////////////////////////////////////////////////
// LAYERS
///////////////////////////////////////////////////////////////////////////////
//Update all modified content (link to files)
function layer_update_all_modified()
{
	var idplacedLayerUpdateAllModified = stringIDToTypeID( "placedLayerUpdateAllModified" );
	executeAction( idplacedLayerUpdateAllModified, undefined, DialogModes.NO );
}

//Open png in a new layer
function layer_to_smartobject()
{
	//Convert to smart object
	var idnewPlacedLayer = stringIDToTypeID( 'newPlacedLayer' );
    executeAction(idnewPlacedLayer, undefined, DialogModes.NO);
	return app.activeDocument.activeLayer;
}

function layer_open_png(layer, file) // TODO : rename to layer_replace_image (not only png)
{
	app.activeDocument.activeLayer = layer;
		
	//Fill with png
	var idplacedLayerReplaceContents = stringIDToTypeID( "placedLayerReplaceContents" );  
    var desc3 = new ActionDescriptor();  
    var idnull = charIDToTypeID( "null" );  
    desc3.putPath( idnull, file);  
    var idPgNm = charIDToTypeID( "PgNm" );  
    desc3.putInteger( idPgNm, 1 );  
	executeAction( idplacedLayerReplaceContents, desc3, DialogModes.NO );
	
	return app.activeDocument.activeLayer;
}

//Function to extract color from a solidfill Layer
function layer_get_colors(layer) 
{ 
	app.activeDocument.activeLayer = layer;
	
	var ref = new ActionReference();  
	ref.putEnumerated( charIDToTypeID("Lyr "), charIDToTypeID("Ordn"), charIDToTypeID("Trgt") );   
	var layerDesc = executeActionGet(ref);  
	var adjList = layerDesc.getList(stringIDToTypeID('adjustment'));  
	var theColors = adjList.getObjectValue(0).getObjectValue(stringIDToTypeID('color'));  
	var str = '';  
	for(var i = 0; i < theColors.count; i++)
	{ 
		//var key = typeIDToStringID(theColors.getKey(i));
		str += theColors.getUnitDoubleValue(theColors.getKey(i)) + ' ';
	}  
	return str;
};

//Fill solidfill layer with color
function layer_fill_color(r, g, b)
{
    var idMk = charIDToTypeID( "Mk  " );  
	var desc15 = new ActionDescriptor();  
	var idnull = charIDToTypeID( "null" );  
	var ref4 = new ActionReference();  
	var idcontentLayer = stringIDToTypeID( "contentLayer" );  
	ref4.putClass( idcontentLayer );  
	desc15.putReference( idnull, ref4 );  
	var idUsng = charIDToTypeID( "Usng" );  
	var desc16 = new ActionDescriptor();  
	var idType = charIDToTypeID( "Type" );  
	var desc17 = new ActionDescriptor();  
	var idClr = charIDToTypeID( "Clr " );  
	var desc18 = new ActionDescriptor();  
	var idRd = charIDToTypeID( "Rd  " );  
	desc18.putDouble( idRd, r);//Red variable  
	var idGrn = charIDToTypeID( "Grn " );  
	desc18.putDouble( idGrn, g);//green variable  
	var idBl = charIDToTypeID( "Bl  " );  
	desc18.putDouble( idBl, b );//blue variable  
	var idRGBC = charIDToTypeID( "RGBC" );  
	desc17.putObject( idClr, idRGBC, desc18 );  
	var idsolidColorLayer = stringIDToTypeID( "solidColorLayer" );  
	desc16.putObject( idType, idsolidColorLayer, desc17 );  
	var idcontentLayer = stringIDToTypeID( "contentLayer" );  
	desc15.putObject( idUsng, idcontentLayer, desc16 );  
	executeAction( idMk, desc15, DialogModes.NO );
	
	return app.activeDocument.activeLayer;
}


function pasteInPlace()
{
	var idpast = charIDToTypeID( "past" );
	var desc557 = new ActionDescriptor();
	var idinPlace = stringIDToTypeID( "inPlace" );
	desc557.putBoolean( idinPlace, true );
	var idAntA = charIDToTypeID( "AntA" );
	var idAnnt = charIDToTypeID( "Annt" );
	var idAnno = charIDToTypeID( "Anno" );
	desc557.putEnumerated( idAntA, idAnnt, idAnno );
	executeAction( idpast, desc557, DialogModes.NO );
}

///////////////////////////////////////////////////////////////////////////////
// MASKS
///////////////////////////////////////////////////////////////////////////////
//Add filter mask
function layer_add_mask_filter(layer, maskType)
{
	//maskType RvlA
	//"RvlA", "HdAl", "RvlS" or "HdSl" 
	if (typeof(maskType) == 'undefined')
		maskType = "RvlA";
	
	app.activeDocument.activeLayer = layer;
	
	alert('applying mask to ' + layer.name);
	
    var idMk = charIDToTypeID( "Mk  " );
	var desc2 = new ActionDescriptor();
	var idNw = charIDToTypeID( "Nw  " );
	var idChnl = charIDToTypeID( "Chnl" );
	desc2.putClass( idNw, idChnl );
	var idAt = charIDToTypeID( "At  " );
	var ref1 = new ActionReference();
	var idChnl = charIDToTypeID( "Chnl" );
	var idMsk = charIDToTypeID( "Msk " );
	ref1.putEnumerated( idChnl, idChnl, idMsk );
	desc2.putReference( idAt, ref1 );
	var idUsng = charIDToTypeID( "Usng" );
	var idUsrM = charIDToTypeID( "UsrM" );
	var idHdAl = charIDToTypeID( maskType );
	desc2.putEnumerated( idUsng, idUsrM, idHdAl );
	executeAction( idMk, desc2, DialogModes.NO );
}

function layer_add_mask_vector(layer)
{
	//TODO : need pathItems, subPath and points
}

//Add layer mask
function layer_add_mask_layer(layer)
{
	app.activeDocument.activeLayer = layer;
	
	var id4556 = charIDToTypeID( "setd" );
	var desc983 = new ActionDescriptor();
	var id4557 = charIDToTypeID( "null" );
	var ref657 = new ActionReference();
	var id4558 = charIDToTypeID( "Chnl" );
	var id4559 = charIDToTypeID( "fsel" );
	ref657.putProperty( id4558, id4559 );
	desc983.putReference( id4557, ref657 );
	var id4560 = charIDToTypeID( "T   " );
	var ref658 = new ActionReference();
	var id4561 = charIDToTypeID( "Chnl" );
	var id4562 = charIDToTypeID( "Chnl" );
	var id4563 = charIDToTypeID( "Trsp" );
	ref658.putEnumerated( id4561, id4562, id4563 );
	desc983.putReference( id4560, ref658 );
	executeAction( id4556, desc983, DialogModes.NO );

	var id4564 = charIDToTypeID( "Mk  " );
	var desc984 = new ActionDescriptor();
	var id4565 = charIDToTypeID( "Nw  " );
	var id4566 = charIDToTypeID( "Chnl" );
	desc984.putClass( id4565, id4566 );
	var id4567 = charIDToTypeID( "At  " );
	var ref659 = new ActionReference();
	var id4568 = charIDToTypeID( "Chnl" );
	var id4569 = charIDToTypeID( "Chnl" );
	var id4570 = charIDToTypeID( "Msk " );
	ref659.putEnumerated( id4568, id4569, id4570 );
	desc984.putReference( id4567, ref659 );
	var id4571 = charIDToTypeID( "Usng" );
	var id4572 = charIDToTypeID( "UsrM" );
	var id4573 = charIDToTypeID( "RvlS" );
	desc984.putEnumerated( id4571, id4572, id4573 );
	executeAction( id4564, desc984, DialogModes.NO );
}

//Remove mask
function remove_mask(layer)
{  
    app.activeDocument.activeLayer = layer;
	
	var idDlt = charIDToTypeID( "Dlt " );  
	var desc21 = new ActionDescriptor();  
	var idnull = charIDToTypeID( "null" );  
	var ref5 = new ActionReference();  
	var idChnl = charIDToTypeID( "Chnl" );  
	var idChnl = charIDToTypeID( "Chnl" );  
	var idMsk = charIDToTypeID( "Msk " );  
	ref5.putEnumerated( idChnl, idChnl, idMsk );  
	desc21.putReference( idnull, ref5 );  
	executeAction( idDlt, desc21, DialogModes.NO );  
}  

//Check if active layer has a layer mask
function layer_has_mask_layer() 
{   
    var hasLayerMask = false;   
    try {   
		var ref = new ActionReference();   
		var keyUserMaskEnabled = app.charIDToTypeID( 'UsrM' );   
		ref.putProperty( app.charIDToTypeID( 'Prpr' ), keyUserMaskEnabled );   
		ref.putEnumerated( app.charIDToTypeID( 'Lyr ' ), app.charIDToTypeID( 'Ordn' ), app.charIDToTypeID( 'Trgt' ) );   
		var desc = executeActionGet( ref );   
		if ( desc.hasKey( keyUserMaskEnabled ) ) {   
			hasLayerMask = true;   
		}   
	}catch(e) {   
		hasLayerMask = false;   
    }   
    return hasLayerMask;   
}   

//Check if active layer has a vector mask
function layer_has_mask_vector() 
{   
    var hasVectorMask = false;   
    try {   
		var ref = new ActionReference();   
		var keyVectorMaskEnabled = app.stringIDToTypeID( 'vectorMask' );   
		var keyKind = app.charIDToTypeID( 'Knd ' );   
		ref.putEnumerated( app.charIDToTypeID( 'Path' ), app.charIDToTypeID( 'Ordn' ), keyVectorMaskEnabled );   
		var desc = executeActionGet( ref );   
		if ( desc.hasKey( keyKind ) ) {   
			var kindValue = desc.getEnumerationValue( keyKind );   
			if (kindValue == keyVectorMaskEnabled) {   
				hasVectorMask = true;   
			}   
		}   
    }catch(e) {   
		hasVectorMask = false;   
    }   
    return hasVectorMask;   
}   


//Check if active layer has a smart filter mask
function layer_has_mask_filter() 
{   
    var hasFilterMask = false;   
    try {   
		var ref = new ActionReference();   
		var keyFilterMask = app.stringIDToTypeID("hasFilterMask");   
		ref.putProperty( app.charIDToTypeID( 'Prpr' ), keyFilterMask);   
		ref.putEnumerated( app.charIDToTypeID( 'Lyr ' ), app.charIDToTypeID( 'Ordn' ), app.charIDToTypeID( 'Trgt' ) );   
		var desc = executeActionGet( ref );   
		if ( desc.hasKey( keyFilterMask ) && desc.getBoolean( keyFilterMask )) {   
			hasFilterMask = true;   
		}	   
    }catch(e) {   
		hasFilterMask = false;   
    }   
    return hasFilterMask;   
}
	
//Select layer mask of active layer
function layer_select_mask_filter() 
{
	try{ 
		var id759 = charIDToTypeID( "slct" );
			var desc153 = new ActionDescriptor();
			var id760 = charIDToTypeID( "null" );
				var ref92 = new ActionReference();
				var id761 = charIDToTypeID( "Chnl" );
				var id762 = charIDToTypeID( "Chnl" );
				var id763 = charIDToTypeID( "Msk " );
				ref92.putEnumerated( id761, id762, id763 );
			desc153.putReference( id760, ref92 );
			var id764 = charIDToTypeID( "MkVs" );
			desc153.putBoolean( id764, false );
		executeAction( id759, desc153, DialogModes.NO );
	}catch(e) {
		alert('exeption select');
	}
}

function layer_select_mask_vector() 
{
	try{ 
		var id55 = charIDToTypeID( "slct" );
		var desc15 = new ActionDescriptor();
		var id56 = charIDToTypeID( "null" );
			var ref13 = new ActionReference();
			var id57 = charIDToTypeID( "Path" );
			var id58 = charIDToTypeID( "Path" );
			var id59 = stringIDToTypeID( "vectorMask" );
			ref13.putEnumerated( id57, id58, id59 );
			var id60 = charIDToTypeID( "Lyr " );
			var id61 = charIDToTypeID( "Ordn" );
			var id62 = charIDToTypeID( "Trgt" );
		ref13.putEnumerated( id60, id61, id62 );
	desc15.putReference( id56, ref13 );
	executeAction( id55, desc15, DialogModes.NO );
	}catch(e) {
		; // do nothing
	}
}

function layer_apply_mask_layer() {
	try{ 
		var id765 = charIDToTypeID( "Dlt " );
			var desc154 = new ActionDescriptor();
			var id766 = charIDToTypeID( "null" );
				var ref93 = new ActionReference();
				var id767 = charIDToTypeID( "Chnl" );
				var id768 = charIDToTypeID( "Ordn" );
				var id769 = charIDToTypeID( "Trgt" );
				ref93.putEnumerated( id767, id768, id769 );
			desc154.putReference( id766, ref93 );
			var id770 = charIDToTypeID( "Aply" );
			desc154.putBoolean( id770, true );
		executeAction( id765, desc154, DialogModes.NO );
	}catch(e) {
	   alert(e + ': on line ' + e.line, 'Script Error', true);
	}
}

function layer_select_raster_contents() 
{
	var id47 = charIDToTypeID( "setd" );
		var desc11 = new ActionDescriptor();
		var id48 = charIDToTypeID( "null" );
			var ref11 = new ActionReference();
			var id49 = charIDToTypeID( "Chnl" );
			var id50 = charIDToTypeID( "fsel" );
			ref11.putProperty( id49, id50 );
		desc11.putReference( id48, ref11 );
		var id51 = charIDToTypeID( "T   " );
			var ref12 = new ActionReference();
			var id52 = charIDToTypeID( "Chnl" );
			var id53 = charIDToTypeID( "Chnl" );
			var id54 = charIDToTypeID( "Trsp" );
			ref12.putEnumerated( id52, id53, id54 );
		desc11.putReference( id51, ref12 );
	executeAction( id47, desc11, DialogModes.NO );
}

//Get points of a vector mask (not used because it takes too much time to read all points
function get_points(path)
{
	//path.select();
	//alert(app.activeDocument.pathItems.length + ' pathitems');
	var res = '';
	alert(path.subPathItems.length + ' subpath');
	for (var i = 0; i < path.subPathItems.length; i++)
	{
		res += (res == '' ? '' : ',') + '[';
		var subPath = path.subPathItems[i];
		alert(subPath.pathPoints.length + ' points');
		return '';
		var pts = '';
		for (var j = 0; j < subPath.pathPoints.length; j++)
		{
			var point = subPath.pathPoints[j];
			pts += (pts == '' ? '' : ',') + '[' + point.anchor[0] + ',' + point.anchor[1] + ']';
		}
		res += pts + ']';
	}
	return res;
}

//TODO : check this 2 methods (not used)
//Unselect mask and select layer image
function layer_unselect_mask()
{
	var id248 = charIDToTypeID( "slct" );
	var desc48 = new ActionDescriptor();
	var id249 = charIDToTypeID( "null" );
	var ref36 = new ActionReference();
	var id250 = charIDToTypeID( "Chnl" );
	var id251 = charIDToTypeID( "Chnl" );
	var id252 = charIDToTypeID( "RGB " );
	ref36.putEnumerated( id250, id251, id252 );
	desc48.putReference( id249, ref36 );
	var id253 = charIDToTypeID( "MkVs" );
	desc48.putBoolean( id253, false );
	executeAction( id248, desc48, DialogModes.NO );
}
//Select mask
function layer_select_mask(LayerName)
{
  try
  {
	var desc = new ActionDescriptor();
	var ref = new ActionReference();
	ref.putEnumerated( charIDToTypeID('Chnl'), charIDToTypeID('Chnl'), charIDToTypeID('Msk ') );
	ref.putName( charIDToTypeID('Lyr '), LayerName );
	desc.putReference( charIDToTypeID('null'), ref );
	desc.putBoolean( charIDToTypeID('MkVs'), true );
	executeAction( charIDToTypeID('slct'), desc, DialogModes.NO );

	// =======================================================
	var id1083 = charIDToTypeID( "setd" );
	var desc238 = new ActionDescriptor();
	var id1084 = charIDToTypeID( "null" );
	var ref161 = new ActionReference();
	var id1085 = charIDToTypeID( "Chnl" );
	var id1086 = charIDToTypeID( "fsel" );
	ref161.putProperty( id1085, id1086 );
	desc238.putReference( id1084, ref161 );
	var id1087 = charIDToTypeID( "T   " );
	var ref162 = new ActionReference();
	var id1088 = charIDToTypeID( "Chnl" );
	var id1089 = charIDToTypeID( "Ordn" );
	var id1090 = charIDToTypeID( "Trgt" );
	ref162.putEnumerated( id1088, id1089, id1090 );
	desc238.putReference( id1087, ref162 );
	executeAction( id1083, desc238, DialogModes.NO );
  }
  catch(e)
  {
	  alert(e)
	  //alert( "This layer has NO layer mask!" );
	  activeDocument.selection.deselect();
  }
}


///////////////////////////////////////////////////////////////////////////////
// MASKS
///////////////////////////////////////////////////////////////////////////////
//TOOLS
function magic_wand(x,y,t,a,c,s) {  
	if(arguments.length < 2) return;// make sure have x,y  
	if(undefined == t) var t = 32;// set defaults of optional arguments  
	if(undefined == a) var a = true;  
	if(undefined == c) var c = false;  
	 if(undefined == s) var s = false;  
	var desc = new ActionDescriptor();  
		var ref = new ActionReference();  
		ref.putProperty( charIDToTypeID('Chnl'), charIDToTypeID('fsel') );  
	desc.putReference( charIDToTypeID('null'), ref );  
		var positionDesc = new ActionDescriptor();  
		positionDesc.putUnitDouble( charIDToTypeID('Hrzn'), charIDToTypeID('#Rlt'), x );// in pixels  
		positionDesc.putUnitDouble( charIDToTypeID('Vrtc'), charIDToTypeID('#Rlt'), y );  
	desc.putObject( charIDToTypeID('T   '), charIDToTypeID('Pnt '), positionDesc );  
	desc.putInteger( charIDToTypeID('Tlrn'), t);// tolerance  
	desc.putBoolean( charIDToTypeID('Mrgd'), s );// sample all layers  
	if(!c) desc.putBoolean( charIDToTypeID( "Cntg" ), false );//  contiguous  
	desc.putBoolean( charIDToTypeID('AntA'), a );// anti-alias  
	executeAction( charIDToTypeID('setd'), desc, DialogModes.NO );  
};  
