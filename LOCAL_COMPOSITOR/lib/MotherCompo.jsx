var MASK_PARAMS = ['layerMaskDensity',
                  'layerMaskFeather',
                  'vectorMaskDensity', 
                  'vectorMaskFeather',
                  'filterMaskDensity',
                  'filterMaskFeather'];


var EMPTY_FILE = '';
var ERROR = false;
var INPUT_PATH = '';

var LOCAL_COMPOSITOR = 'C:/LOCAL_COMPOSITOR/';
var EMPTY_PNG = 'empty/_empty.png';
var OPEN_COMPO = 'open_compo.bat';
var SAVE_COMPO = 'save_compo.bat';


///////////////////////////////////////////////////////////////////////////////
// SAVING 
///////////////////////////////////////////////////////////////////////////////
function save_mother_compo(file)
{
	//Get empty png
	var emptyPath = LOCAL_COMPOSITOR + EMPTY_PNG;
	var emptyFile = new File(emptyPath);
	if (!emptyFile.exists)
	{
		alert(emptyPath + ' not found');
		return false;
	}
	
	//Save xml
	Log('saving ' + file.fullName);
	if (!save_compo_xml(file, emptyFile))
	{
		alert('error saving xml');
		return false;
	}
	
	//Save compo empty
	var psbPath = file.fullName.replace(FILE_EXT_XML, FILE_EXT_PSB);
	Log('saving ' + psbPath);
	if (!save_compo_empty(psbPath))
	{
		alert('error saving psb');
		return false;
	}
	
	var outputDir = psbPath.substring(0, psbPath.lastIndexOf('/') + 1);
	
	//Copy open_compo.bat in mother compo dir
	var openBat = new File(LOCAL_COMPOSITOR + OPEN_COMPO);
	if (!openBat.copy(outputDir + OPEN_COMPO))
	{
		alert('error copying ' + OPEN_COMPO);
		return false;
	}
	
	//Copy save_compo.bat in mother compo dir
	var saveBat = new File(LOCAL_COMPOSITOR + SAVE_COMPO);
	if (!saveBat.copy(outputDir + SAVE_COMPO))
	{
		alert('error copying ' + SAVE_COMPO);
		return false;
	}
	
	return true;
}

//Save mother compo (active document) to xml file
function save_compo_xml(file, emptyFile)
{
	EMPTY_FILE = emptyFile;
	
	var xml = new XML('<mother_compo></mother_compo>');
	
	//Parse active doc
	var doc = app.activeDocument;
	
	//Save channels
	var listNode = xml_create_node(xml, 'channels');
//	We don't save channels any more (they are in the psd)
//	xml_save_list(doc, 'channels', listNode, ['name', 'typename', 'kind'], false, on_channel_parsed, on_node_channel_created);
	
	//Save layerComps
	listNode = xml_create_node(xml, 'layerComps');
//	We don't save layer comps any more (they are in the psd)
//	xml_save_list(doc, 'layerComps', listNode, ['name', 'typename', 'appearance', 'position', 'visibility'], false);
	
	app.activeDocument.selection.selectAll();
	
	//Save layers hierarchy and settings
	listNode = xml_create_node(xml, 'layers');
	xml_save_list(doc, 'layers', listNode, ['name', 'typename', 'kind', 'blendMode', 'opacity', 'visible']
		, true, null, on_node_layer_created);
	
	//Write to file
	if (!file.open('w'))
		return false;
	
	file.write(xml.toXMLString());
	file.close();
	return true;
}

function save_compo_empty(filePath)
{
	//clean_layers(app.activeDocument);
	if (!save_psb(filePath))
	{
		LogError('FATAL ERROR SAVING EMPTY COMPO (PSB) ' + output + FILE_EXT_PSB);
		return false;
	}
	
	return true;
}

//Callback when parsing channel
function on_channel_parsed(channel)
{
	if (channel.kind == ChannelType.COMPONENT)
	{	//We don't save component channels
		return false;
	}
	return true;
}

//Callbacks when channel node created
function on_node_channel_created(channel, xmlNode)
{	
	//Add specific channel info
	xmlNode.@color = channel.color.rgb.red + ' ' + channel.color.rgb.green + ' ' + channel.color.rgb.blue;
	channel.visible = true; // set channel visible to get histogram
	xmlNode.@histogram = channel.histogram;

	if (channel.kind == ChannelType.MASKEDAREA || channel.kind == ChannelType.SELECTEDAREA)
	{
		xmlNode.@opacity = channel.opacity;
	}
}

//Callbacks when layer node created
function on_node_layer_created(layer, xmlNode)
{
	app.activeDocument.activeLayer = layer;
	layer.allLocked = false;
	
	Log('saving ' + layer.name);
	
	if (layer.kind == LayerKind.SOLIDFILL)
	{
		xmlNode.@color = layer_get_colors(layer);
		return;
	}
	
	//Mask infos
	xmlNode.@hasLayerMask = layer_has_mask_layer();
	xmlNode.@hasVectorMask = layer_has_mask_vector();
	xmlNode.@hasFilterMask = layer_has_mask_filter();
	
	if (xmlNode.@hasLayerMask == true)
	{
		xmlNode.@layerMaskDensity = layer.layerMaskDensity;
		xmlNode.@layerMaskFeather = layer.layerMaskFeather;
	}
	if (xmlNode.@hasVectorMask == true)
	{
		xmlNode.@vectorMaskDensity = layer.vectorMaskDensity;
		xmlNode.@vectorMaskFeather = layer.vectorMaskFeather;
	}
	if (xmlNode.@hasFilterMask == true)
	{
		xmlNode.@filterMaskDensity = layer.filterMaskDensity;
		xmlNode.@filterMaskFeather = layer.filterMaskFeather;
	}
	
	/* test to get points (-> too many points ^^)
	if (xmlNode.@hasVectorMask == true)
	{
		var path = app.activeDocument.pathItems[0];
		
		//xmlNode.@pathItem = get_points(path);
	}
	*/
	
	clean_layer(layer);
}

function clean_layer(layer)
{
	if (layer.typename != 'ArtLayer')
		return;
	
	//layer.rasterize(RasterizeType.ENTIRELAYER);
	
	try { 
		var index = layer.name.indexOf(' '); if (index < 0) index = layer.name.length;
		var fileName = layer.name.substring(0, index) + FILE_EXT_PNG;
		var destEmpty = COMPO_INPUT + '/' + fileName;
		EMPTY_FILE.copy(destEmpty);
		if (fileName.substring(0, 3) == 'ID_')
			layer = layer_to_smartobject();
		layer = layer_open_png(layer, new File(destEmpty));
		//app.activeDocument.activeLayer = layer;
		//app.activeDocument.selection.clear();
	} catch (e) 
	{
		//alert("can't replace " + fileName);
	}
}

///////////////////////////////////////////////////////////////////////////////
// OPENING 
///////////////////////////////////////////////////////////////////////////////
//Open mother compo from xml file into current document
function open_mother_compo(file, inputPath)
{
	INPUT_PATH = inputPath;
	
	//Read local config
	if (!file.open('r'))
		return false;
	
	//Parse xml file
	var content = file.read();
	file.close();
	var xml = new XML(content);
	
	//Create doc
	//var doc = create_document();
	var doc = app.activeDocument;
	
	//Read layer hierarchy
	ERROR = false;
	xml_read_list(xml.channels, doc, 'channel', on_channel_created);
	xml_read_list(xml.layerComps, doc, 'layerComp', on_layerComp_created);
	xml_read_list(xml.layers, doc, 'layer', on_layer_created, true);
	
	//Remove default layer
	//doc.artLayers[0].remove();
	
	//Create masks at the end (or it throws errors...)
	//create_masks(doc);
	
	return !ERROR;
}

//Get list from parent to add a new element
function elem_get_list(parent, xmlNode)
{
	//Convert typename to listName
	var listName = xmlNode.@typename;
	listName = listName.substring(0, 1).toLowerCase() + listName.substring(1) + 's';
		
	//Return list
	return parent[listName];
}

//Callback to create a channel
function on_channel_created(parent, xmlNode)
{
	var list = elem_get_list(parent, xmlNode);
	//var elem = list.add();
	var elem = list.getByName(xmlNode.@name);
	elem_fill_attr(elem, xmlNode);
	return elem;
}

//Callback to create a layerComp
function on_layerComp_created(parent, xmlNode)
{
	var list = elem_get_list(parent, xmlNode);
	//var elem = list.add('', '', false, false, false); // fake parameters required
	var elem = list.getByName(xmlNode.@name);
	elem_fill_attr(elem, xmlNode);
	return elem;
}

//Callback to create a layer (we don't create them any more, just set image and settings)
function on_layer_created(parent, xmlNode)
{
	var list = elem_get_list(parent, xmlNode);
	
	var elem = null;
	try {
		elem = list.getByName(xmlNode.@name);
	}
	catch(e)
	{
		try {
			elem = list.getByName('SK_'+xmlNode.@name);
		}
		catch(e)
		{
			LogError('Layer ' + xmlNode.@name + ' not found in ' + parent.name);
			ERROR = true;
			return;
		}
	}
	
	app.activeDocument.activeLayer = elem;
		
	//Log('create ' + xmlNode.@name);
	
	var creationOk = true;
	
	//Layer group
	if (xmlNode.@typename == 'LayerSet')
		;//elem = list.add();
	
	//Fill color
	else if (xmlNode.@kind == 'LayerKind.SOLIDFILL')
	{
		var colors = xmlNode.@color.split(' ');
		//elem = layer_fill_color(colors[0], colors[1], colors[2]);
	}
	
	//Art layer
	else if (xmlNode.@kind == 'LayerKind.SMARTOBJECT')
	{
		//elem = list.add();
		var dir = INPUT_PATH + '/';
		var fileName = '' + xmlNode.@name;
		var index = fileName.indexOf(' '); if (index < 0) index = fileName.length;
		fileName = fileName.substring(0, index) + FILE_EXT_PNG;
		var file = new File(dir + fileName);
		if (!file.exists)
			file = new File(dir + 'SK_' + fileName);
		if (!file.exists)
			file = new File(dir + 'ID_' + fileName);
		if (file.exists)
		{
			elem = layer_open_png(elem, file);
			//Log(fileName + ' opened');
		}
		else //TODO : list all names to report to good errors
		{
			LogError('OPEN_COMPO : PNG ' + fileName + ' not found for layer ' + xmlNode.@name);
			creationOk = false;
			ERROR = true;
		}
	}
	
	elem_fill_attr(elem, xmlNode);
	
	//if (creationOk)
	//	create_mask(elem);
	
	return elem;
}

//Fill layer attributes
function elem_fill_attr(elem, elemNode)
{
	var type = elemNode.@typename;
	
	//Read params
	var attrs = elemNode.attributes();
	for (var j = 0; j < attrs.length(); j++)
	{
		var attr = attrs[j];
		var name = '' + attr.name();
		if (name == 'typename')
			continue;
		var val = attr;
		
		if (val == 'undefined') continue;
		
		//mask params will be set after
		if (name.indexOf('Density') >= 0 || name.indexOf('Feather') >= 0)
		{
			elem['mask_' + name] = parseFloat(val);
			continue;
		}
		
		if (type == 'Channel')
		{
			if (name == 'kind')
				val = ChannelType[val.substring(val.indexOf('.')+1)];
			else if (name == 'color')
			{
				var col = new SolidColor();
				var colors = val.split(' ');
				col.rgb.red = colors[0];
				col.rgb.green = colors[1];
				col.rgb.blue = colors[2];
				val = col;
			}			
			else if (name == 'histogram')
				val = val.split(',');
		}	
		else if (name == 'kind')
		{	//Convert to enum
			val = LayerKind[val.substring(val.indexOf('.')+1)];
			elem['realkind'] = val; // store real kind
			if (val != LayerKind.NORMAL && val != LayerKind.TEXT) // lol... can't change type
				val = LayerKind.NORMAL;
			continue;
		}
		if (name == 'blendMode')
			val = BlendMode[val.substring(val.indexOf('.')+1)];
		else if (name == 'opacity')
			val = parseFloat(val);
		else if (val == 'false' || val == 'true')
			val = (val == 'true'); // force boolean
		
		//Apply parameter
		//Log(name+'='+val);
		elem[name] = val;
	}
}

//Recursive method to create masks (not used any more, masks are in the psd)
function create_masks(doc)
{
	if (doc.layers)
	{
		for (var i = 0; i < doc.layers.length; i++)
		{
			var layer = doc.layers[i];
			create_mask(layer);
			create_masks(layer);
		}
	}
}

function create_mask(layer)
{	
	//Add mask & apply params
	if (layer.hasLayerMask == 1)	
		layer_add_mask_layer(layer);
	if (layer.hasVectorMask == 1)	
		layer_add_mask_vector(layer);
	if (layer.hasFilterMask == 1)	
		layer_add_mask_filter(layer);
	
	return;
	for (var i = 0; i < MASK_PARAMS.length; i++)
	{
		var key = MASK_PARAMS[i];
		if (layer.hasOwnProperty('mask_'+key))
			layer[key] = layer['mask_'+key];
	}
}