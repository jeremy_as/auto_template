//Helper to create xml node with attributes
function xml_create_node(parent, tag, attributes, names)
{
	var attr = ' ';
	if (names)
	{
		for (var i = 0; i < names.length; i++)
		{
			var key = names[i];
			if (attributes.hasOwnProperty(key))
				attr += key + '="' + attributes[key] + '" ';
		}
		
		//TODO : full forbidden list
		attr = attr.replace(/&/g, 'AND'); // character '&' is forbidden in xml attributes 
	}
	
	//Log('<'+tag+attr+'></'+tag+'>');
	
	var node = new XML('<'+tag+attr+'></'+tag+'>');
	parent.appendChild(node);
	return node;
}

//Save object member list with their attributes to xml nodes, recursively
//Usage : save_list(objectToSave, nameOfMemberList, xmlParentNode, ['attr1', 'attr2', 'attr3']);
function xml_save_list(obj, listName, parentNode, attrNames, withChildren, parsingCallBack, createdCallBack)
{
	if (!obj.hasOwnProperty(listName))
		return;
	
	var list = obj[listName];
	var len = list.length;
	var elemName = listName.substring(0, listName.length - 1); // remove 's' for single element
	
	for (var i = len - 1; i >= 0; i--)
	{
		var elem = list[i];
		
		//Parsing Callback
		if (typeof(parsingCallBack) == 'function')
			if (parsingCallBack(elem) === false) 
				continue; //skip this one
		
		//Create node
		var elemNode = xml_create_node(parentNode, elemName, elem, attrNames);
		
		//Creation Callback
		if (typeof(createdCallBack) == 'function')
			createdCallBack(elem, elemNode);
				
		//Save children recursively
		if (withChildren)
			xml_save_list(elem, listName, elemNode, attrNames, withChildren, parsingCallBack, createdCallBack);
	}
}

//Read xml nodes to construct hierarchy (with some specifics for Photoshop layers collection)
function xml_read_list(xml, obj, nodeName, createdCallBack, withChildren)
{
	if (!xml.hasOwnProperty(nodeName))
		return;
	
	var len = xml[nodeName].length();
	for (var i = 0; i < len; i++)
	{
		var elemNode = xml[nodeName][i];

		//Callback for creation
		var elem = createdCallBack(obj, elemNode);
		
		//Read children recursively
		if (withChildren)
			xml_read_list(elemNode, elem, nodeName, createdCallBack, withChildren);
	}
}

