var LOG_PATH = 'C:/LOCAL_COMPOSITOR/log/';
var LOG_OPEN_ERROR = LOG_PATH + 'open_error.bat';
var LOG_OPEN_OUTPUT = LOG_PATH + 'open_output.bat';

var LOG_NAME = 'output';
var LOG_NAME_ERROR = 'error';
var LOG_EXT = '.log';

var LOG_FILE = LOG_PATH + LOG_NAME + LOG_EXT;
var LOG_FILE_ERROR = LOG_PATH + LOG_NAME_ERROR + LOG_EXT;

function Log(msg, clean)
{
	LogFile(LOG_FILE, msg, clean);
}

function LogError(msg, clean)
{
	LogFile(LOG_FILE_ERROR, msg, clean);
}

function LogFile(file, msg, clean)
{
	var logFile = new File(file);
	logFile.open(clean === true ? 'w' : 'a');
	logFile.writeln();
	logFile.write((new Date()) + ' -> ' + msg);
	logFile.close();
}

function LogOpen()
{
	var file = new File(LOG_OPEN_OUTPUT);
	file.execute();
}

function LogErrorOpen()
{
	var file = new File(LOG_OPEN_ERROR);
	file.execute();
}