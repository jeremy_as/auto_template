﻿///////////////////////////////////////////////////////////////////////////////
// LIBS
///////////////////////////////////////////////////////////////////////////////

#include "C:/LOCAL_COMPOSITOR/lib/_includes.jsx"

///////////////////////////////////////////////////////////////////////////////
// MAIN 
///////////////////////////////////////////////////////////////////////////////
function main()
{
	version_check();
	
	//Remember ruler units; switch to pixels
	var originalRulerUnits = preferences.rulerUnits;
	preferences.rulerUnits = Units.PIXELS;
	
	//Set current folder (directory of launch)
	Folder.current = $.fileName.substring(0, $.fileName.lastIndexOf('/')) + '/';

	//Avoid mislaunch from server
	if (new Folder('lib').exists)
	{
		alert('COPY AUTOTEMPLATE.bat IN A LOCAL WORKING DIRECTORY');
		return;
	}
	
	//Init log
	Log('launch', true);
	LogError('', true);

	//Create compositor
	var compositor = new Compositor();

	//Let's go
	var allOk = compositor.start();
	
	//Restore original ruler unit
	preferences.rulerUnits = originalRulerUnits;
	
	//All done
	var msgError = !allOk ? ' with errors : see error.log' : ' without errors';
	Log('done' + msgError);
	alert('done' + msgError);
	
	//Show log
	if (!allOk)
	{
		if (confirm('Do you want to open error log?'))
			LogErrorOpen();
	}
	else if (confirm('Do you want to open output log?'))
		LogOpen();
}


///////////////////////////////////////////////////////////////////////////////
// ERROR HANDLING
///////////////////////////////////////////////////////////////////////////////
function handleError(e)
{
	// don't report error on user cancel
	if (e.number == 8007) return;
		
	if (confirm('An unknown error has occurred.\n' + 'Would you like to see more information?', true, 'Unknown Error'))
		alert(e + ': on line ' + e.line, 'Script Error', true);
}

///////////////////////////////////////////////////////////////////////////////
// MAIN CALL
///////////////////////////////////////////////////////////////////////////////
try { main() } catch (e) { handleError(e) }