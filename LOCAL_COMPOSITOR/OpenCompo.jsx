///////////////////////////////////////////////////////////////////////////////
// LIBS
///////////////////////////////////////////////////////////////////////////////

#include "C:/LOCAL_COMPOSITOR/lib/_includes.jsx"

///////////////////////////////////////////////////////////////////////////////
// MAIN 
///////////////////////////////////////////////////////////////////////////////
function main()
{
	version_check();
	
	//Init log
	Log('', true);
	LogError('', true);
		
	//Set current folder (directory of launch)
	Folder.current = $.fileName.substring(0, $.fileName.lastIndexOf('/')) + '/';
	
	//Check psb file
	var files = file_get_all(Folder.current, "*" + FILE_EXT_PSB);
	if (files.length === 0)
	{
		alert (FILE_EXT_PSB + ' file not found in ' + Folder.current);
		return;
	}
	
	if (files.length > 1)
	{
		alert ('WARNING: multiple PSB files in ' + Folder.current + ', only the first will be opened : ' + files[0].name);
	}
	
	var file = files[0];
	
	//Check xml file
	var xmlPath = file.path + '/' + file.name.replace(FILE_EXT_PSB, FILE_EXT_XML);
	var xmlFile = new File(xmlPath);
	if (!xmlFile.exists)
	{
		alert (FILE_EXT_XML + ' ' + xmlFile.fsName + ' not found in ' + Folder.current);
		return;
	}
	
	//Find config.xml to get render path
	var currDir = Folder.current;
	var confFile = new File(CONF_FILE);
	var i = 0;
	while (i++ < 5)
	{
		if (!confFile.exists)
		{
			 Folder.current = Folder.current + '/../';
			 confFile = new File(CONF_FILE);
		}
		else 
			break;
	}
	if (!confFile.exists)
	{
		alert ('config.xml not found! Please init collection directory before opening compo in it');
		return;
	}
	var isSpec = xmlPath.indexOf('SPEC_COMPO') >= 0;
	var isShadow = xmlPath.indexOf('/SH_') >= 0;
	
	//Load xml of the collection
	var compositor = new Compositor();
	if (!compositor.loadXml())
	{
		alert('error loading xml');
		return; // something went wrong, see error.log for details
	}
		
	//Read render path and update collection folders
	if (!compositor.readRenderPath())
	{
		if (confirm('Do you want to open error log?'))
			LogErrorOpen();
		return; // something went wrong, see error.log for details
	}
	
	try {
		if (isShadow)
		{
			var type = xmlPath.substring(xmlPath.indexOf('/SH_') + 4);
			type = type.substring(0, type.indexOf('/'));
		}
		
		Log('loading ' + (isShadow? 'shadow ' + type : (isSpec ? 'spec compo' : 'mother compo')) + ' ' + file.fsName);
			
		var angle = null;
		if (isShadow)
			angle = compositor.root.getFirstChild() //model
			.getFirstChild() // pose
			.getFirstShadow(type)
			.getFirstChild();
		else if (isSpec)
		{
			var array = file.fullName.split('/');
			var skin = array[array.length - 4];
			var pose = array[array.length - 3];
			var angle = array[array.length - 2];
			angle = compositor.root.children[skin].children[pose].getFirstChild().children[angle];
		}
		else
			angle = compositor.root.getFirstChild() //model
			.getFirstChild() // pose
			.getFirstChild() // variation
			.getFirstChild(); // angle
	}
	catch (e) { var msg = 'FATAL ERROR GETTING ANGLE'; LogError(msg); alert(msg); return;}
	
	if (!confirm('opening ' + (isShadow ? 'shadow ' + type + ' ': '') + angle.getPath()))
		return;
	
	if (isShadow)
	{
		var renderPath = compositor.getRenderPath(angle, true);
		if (xmlPath.indexOf('/SH_FLOOR') < 0)
			renderPath += 'L/';
		compositor.copyToLocal(angle, renderPath);
	}
	else
		compositor.copyRenderFiles(angle);

	//Copy psb to input dir
	var psbPath = COMPO_INPUT + '/' + file.name;
	file.copy(psbPath);
	
	//Open psb
	open_psb(psbPath);
	
	//Open xml
	Log('opening ' + xmlFile.fsName);
	var ok = open_mother_compo(xmlFile, COMPO_INPUT);
	
	var msgError = !ok ? ' with errors : see error.log' : ' without errors';
	Log('done' + msgError);
	
	//Show log
	if (!ok)
	{
		alert('An error occured');
		if (confirm('Do you want to open error log?'))
			LogErrorOpen();
	}
	else
		alert('compo ' + xmlFile.fsName + ' opened'); 
}

///////////////////////////////////////////////////////////////////////////////
// ERROR HANDLING
///////////////////////////////////////////////////////////////////////////////
function handleError(e)
{
	// don't report error on user cancel
	if (e.number == 8007) return;
		
	if (confirm('An unknown error has occurred.\n' + 'Would you like to see more information?', true, 'Unknown Error'))
		alert(e + ': on line ' + e.line, 'Script Error', true);
}

///////////////////////////////////////////////////////////////////////////////
// MAIN CALL
///////////////////////////////////////////////////////////////////////////////

var currDir = Folder.current;
try 
{ 
	main();
	Folder.current = currDir;
} 
catch (e) 
{ 
	Folder.current = currDir;
	handleError(e);
}